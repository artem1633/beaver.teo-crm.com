<?php

use yii\db\Migration;

/**
 * Class m200910_012050_change_bid_column
 */
class m200910_012050_change_bid_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('bid', 'contract_num', $this->string()->comment('Номер договора'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('bid', 'contract_num', $this->integer()->comment('Номер договора'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200910_012050_change_bid_column cannot be reverted.\n";

        return false;
    }
    */
}
