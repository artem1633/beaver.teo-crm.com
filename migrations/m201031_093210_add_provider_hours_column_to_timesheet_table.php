<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%timesheet}}`.
 */
class m201031_093210_add_provider_hours_column_to_timesheet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('timesheet', 'provider_hours', $this->integer()->after('hours')->comment('Часы поставщика'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('timesheet', 'provider_hours');
    }
}
