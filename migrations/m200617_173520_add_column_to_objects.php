<?php

use yii\db\Migration;

/**
 * Class m200617_173520_add_column_to_objects
 */
class m200617_173520_add_column_to_objects extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('objects', 'name', $this->string()->comment('Название объекта'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('objects', 'name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200617_173520_add_column_to_objects cannot be reverted.\n";

        return false;
    }
    */
}
