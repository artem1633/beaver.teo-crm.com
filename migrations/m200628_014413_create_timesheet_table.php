<?php

use yii\db\Migration;

/**
 * Handles the creation of table `timesheet`.
 */
class m200628_014413_create_timesheet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('timesheet', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->comment('Дата'),
            'hours' => $this->integer()->comment('Кол-во часов'),
            'bid_id' => $this->integer()->comment('id Заявки')
        ]);
        $this->createIndex(
            'idx-timesheet-bid_id',
            'timesheet',
            'bid_id'
        );
        $this->addForeignKey(
            'fk-timesheet-bid_id',
            'timesheet',
            'bid_id',
            'bid',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-timesheet-bid_id',
            'timesheet'
        );
        $this->dropIndex(
            'idx-timesheet-bid_id',
            'timesheet'
        );
        $this->dropTable('timesheet');
    }
}
