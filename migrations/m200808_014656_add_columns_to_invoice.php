<?php

use yii\db\Migration;

/**
 * Class m200808_014656_add_columns_to_invoice
 */
class m200808_014656_add_columns_to_invoice extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn('invoice', 'date_from', $this->date()->comment('Период от'));
        $this->addColumn('invoice', 'date_to', $this->date()->comment('Период по'));
        $this->addColumn('invoice', 'upd_date', $this->date()->comment('Дата УПД'));
        $this->addColumn('invoice', 'upd_num', $this->integer()->comment('Сумма УПД'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('invoice', 'date_from');
        $this->dropColumn('invoice', 'date_to');
        $this->dropColumn('invoice', 'upd_date');
        $this->dropColumn('invoice', 'upd_sum');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200808_014656_add_columns_to_invoice cannot be reverted.\n";

        return false;
    }
    */
}
