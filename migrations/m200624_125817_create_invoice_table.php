<?php

use yii\db\Migration;

/**
 * Handles the creation of table `invoice`.
 */
class m200624_125817_create_invoice_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('invoice', [
            'id' => $this->primaryKey(),
            'num' => $this->string()->comment('Номер счета'),
            'invoice_date' => $this->date()->comment('Дата счета'),
            'status' => $this->string()->comment('Статус оплаты'),
            'pay_date'=>$this->date()->comment('Дата оплаты'),
            'bid_id' => $this->integer()->comment('id заявки')
        ]);
        $this->createIndex(
            'idx-invoice-bid_id',
            'invoice',
            'bid_id'
        );

        $this->addForeignKey(
            'fk-invoice-bid_id',
            'invoice',
            'bid_id',
            'bid',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-invoice-bid_id',
            'invoice'
        );
        $this->dropIndex(
            'idx-invoice-bid_id',
            'invoice'
        );
        $this->dropTable('invoice');
    }
}
