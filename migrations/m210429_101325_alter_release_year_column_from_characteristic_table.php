<?php

use yii\db\Migration;

/**
 * Class m210429_101325_alter_release_year_column_from_characteristic_table
 */
class m210429_101325_alter_release_year_column_from_characteristic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('characteristic', 'release_year', $this->string()->comment('Год выпуска'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
