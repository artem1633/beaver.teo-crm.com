<?php

use yii\db\Migration;

/**
 * Class m200625_124922_delete_timesheet
 */
class m200625_124922_delete_timesheet extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-timesheet-type_subgroup_id',
            'timesheet'
        );

        $this->dropIndex(
            'idx-timesheet-type_subgroup_id',
            'timesheet'
        );
        $this->dropForeignKey(
            'fk-timesheet-technic_type_id',
            'timesheet'
        );

        $this->dropIndex(
            'idx-timesheet-technic_type_id',
            'timesheet'
        );
        $this->dropForeignKey(
            'fk-timesheet-technic_id',
            'timesheet'
        );

        $this->dropIndex(
            'idx-timesheet-technic_id',
            'timesheet'
        );
        $this->dropForeignKey(
            'fk-timesheet-bid_status_id',
            'timesheet'
        );

        $this->dropIndex(
            'idx-timesheet-bid_status_id',
            'timesheet'
        );
        $this->dropForeignKey(
            'fk-timesheet-object_id',
            'timesheet'
        );

        $this->dropIndex(
            'idx-timesheet-object_id',
            'timesheet'
        );
        $this->dropForeignKey(
            'fk-timesheet-company_id',
            'timesheet'
        );

        $this->dropIndex(
            'idx-timesheet-company_id',
            'timesheet'
        );
        $this->dropTable('timesheet');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createTable('timesheet', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('id Контрагента'),
            'object_id' => $this->integer()->comment('id Объекта'),
            'period_from' => $this->date()->comment('Период с'),
            'period_to' => $this->date()->comment('Период по'),
            'bid_status_id' => $this->integer()->comment('Статус заявки'),
            'technic_id' => $this->integer()->comment('Техника'),
            'technic_type_id' => $this->integer()->comment('Тип техники'),
            'type_subgroup_id' => $this->integer()->comment('Подгруппа')
        ]);
        $this->createIndex(
            'idx-timesheet-company_id',
            'timesheet',
            'company_id'
        );

        $this->addForeignKey(
            'fk-timesheet-company_id',
            'timesheet',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-timesheet-object_id',
            'timesheet',
            'object_id'
        );

        $this->addForeignKey(
            'fk-timesheet-object_id',
            'timesheet',
            'object_id',
            'objects',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-timesheet-bid_status_id',
            'timesheet',
            'bid_status_id'
        );

        $this->addForeignKey(
            'fk-timesheet-bid_status_id',
            'timesheet',
            'bid_status_id',
            'bid_status',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-timesheet-technic_id',
            'timesheet',
            'technic_id'
        );

        $this->addForeignKey(
            'fk-timesheet-technic_id',
            'timesheet',
            'technic_id',
            'technic',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-timesheet-technic_type_id',
            'timesheet',
            'technic_type_id'
        );

        $this->addForeignKey(
            'fk-timesheet-technic_type_id',
            'timesheet',
            'technic_type_id',
            'technic_type',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-timesheet-type_subgroup_id',
            'timesheet',
            'type_subgroup_id'
        );

        $this->addForeignKey(
            'fk-timesheet-type_subgroup_id',
            'timesheet',
            'type_subgroup_id',
            'technic_type_subgroup',
            'id',
            'SET NULL'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200625_124922_delete_timesheet cannot be reverted.\n";

        return false;
    }
    */
}
