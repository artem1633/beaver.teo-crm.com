<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%bid}}`.
 */
class m201031_075916_add_new_columns_to_bid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'provider_rent_sum', $this->float()->comment('Сумма аренды поставщика'));
        $this->addColumn('bid', 'provider_delivery_sum', $this->float()->comment('Сумма доставки поставщика'));
        $this->addColumn('bid', 'provider_contract_num', $this->string()->comment('Номер договора поставщика'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'provider_rent_sum');
        $this->dropColumn('bid', 'provider_delivery_sum');
        $this->dropColumn('bid', 'provider_contract_num');
    }
}
