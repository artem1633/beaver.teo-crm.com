<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technic_type`.
 */
class m200603_015053_create_technic_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('technic_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Вид Техники')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('technic_type');
    }
}
