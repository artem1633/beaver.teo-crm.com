<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%scan}}`.
 */
class m201031_084136_add_company_id_column_to_scan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('scan', 'company_id', $this->integer()->comment('Компания'));

        $this->createIndex(
            'idx-scan-company_id',
            'scan',
            'company_id'
        );

        $this->addForeignKey(
            'fk-scan-company_id',
            'scan',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-scan-company_id',
            'scan'
        );

        $this->dropIndex(
            'idx-scan-company_id',
            'scan'
        );

        $this->dropColumn('scan', 'company_id');
    }
}
