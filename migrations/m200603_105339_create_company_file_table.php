<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_file`.
 */
class m200603_105339_create_company_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%company_file}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'type' => $this->integer()->comment('Тип'),
            'name' => $this->string()->comment('Наименование'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-company_file-company_id',
            'company_file',
            'company_id'
        );

        $this->addForeignKey(
            'fk-company_file-company_id',
            'company_file',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-company_file-company_id',
            'company_file'
        );

        $this->dropIndex(
            'idx-company_file-company_id',
            'company_file'
        );

        $this->dropTable('{{%company_file}}');
    }
}
