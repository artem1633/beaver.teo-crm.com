<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object`.
 */
class m200603_010841_create_objects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('objects', [
            'id' => $this->primaryKey(),
            'address' => $this->string()->comment('Адрес объекта'),
            'contact_name' => $this->string()->comment('Контактное лицо'),
            'contact_phone' => $this->string()->comment('Тел. контактного лицв'),
            'access_time' => $this->string()->comment('Время работы/доступа'),
            'company_id' => $this->integer()->comment('Компания')
        ]);
        $this->createIndex(
            'idx-objects-company_id',
            'objects',
            'company_id'
        );

        $this->addForeignKey(
            'fk-objects-company_id',
            'objects',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-objects-company_id',
            'objects'
        );

        $this->dropIndex(
            'idx-objects-company_id',
            'objects'
        );
        $this->dropTable('objects');
    }
}
