<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%bid}}`.
 */
class m201220_105429_add_executor_contract_column_to_bid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'executor_contract', $this->string()->comment('Договор с поставщиком'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'executor_contract');
    }
}
