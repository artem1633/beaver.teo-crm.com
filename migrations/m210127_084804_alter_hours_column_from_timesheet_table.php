<?php

use yii\db\Migration;

/**
 * Class m210127_084804_alter_hours_column_from_timesheet_table
 */
class m210127_084804_alter_hours_column_from_timesheet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('timesheet', 'hours', $this->float()->comment('Кол-во часов'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
