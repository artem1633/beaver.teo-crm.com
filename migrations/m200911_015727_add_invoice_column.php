<?php

use yii\db\Migration;

/**
 * Class m200911_015727_add_invoice_column
 */
class m200911_015727_add_invoice_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'sum', $this->integer()->comment('Сумма счета'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('invoice', 'sum');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200911_015727_add_invoice_column cannot be reverted.\n";

        return false;
    }
    */
}
