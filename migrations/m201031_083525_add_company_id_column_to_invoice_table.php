<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%invoice}}`.
 */
class m201031_083525_add_company_id_column_to_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'company_id', $this->integer()->comment('Компания'));

        $this->createIndex(
            'idx-invoice-company_id',
            'invoice',
            'company_id'
        );

        $this->addForeignKey(
            'fk-invoice-company_id',
            'invoice',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-invoice-company_id',
            'invoice'
        );

        $this->dropIndex(
            'idx-invoice-company_id',
            'invoice'
        );

        $this->dropColumn('invoice', 'company_id');
    }
}
