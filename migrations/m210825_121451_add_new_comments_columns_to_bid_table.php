<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%bid}}`.
 */
class m210825_121451_add_new_comments_columns_to_bid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'comment_diller', $this->text()->comment('Комментарий поставщика'));
        $this->addColumn('bid', 'comment_client', $this->text()->comment('Комментарий заказчика'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'comment_diller');
        $this->dropColumn('bid', 'comment_client');
    }
}
