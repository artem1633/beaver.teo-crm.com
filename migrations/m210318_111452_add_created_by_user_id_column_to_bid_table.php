<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%bid}}`.
 */
class m210318_111452_add_created_by_user_id_column_to_bid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'created_by_user_id', $this->integer()->comment('Автор (пользователь)'));

        $this->createIndex(
            'idx-bid-created_by_user_id',
            'bid',
            'created_by_user_id'
        );

        $this->addForeignKey(
            'fk-bid-created_by_user_id',
            'bid',
            'created_by_user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-bid-created_by_user_id',
            'bid'
        );

        $this->dropIndex(
            'idx-bid-created_by_user_id',
            'bid'
        );

        $this->dropColumn('bid', 'created_by_user_id');
    }
}
