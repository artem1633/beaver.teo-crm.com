<?php

use yii\db\Migration;

/**
 * Class m200610_142337_add_column_to_bid
 */
class m200610_142337_add_column_to_bid extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'executor_id', $this->integer()->comment('Исполнитель'));
        $this->addColumn('bid', 'type_id', $this->integer()->comment('Тип техники'));
        $this->addColumn('bid', 'subgroup_id', $this->integer()->comment('Подгруппа'));
        $this->addColumn('bid', 'delivery_from', $this->date()->comment('Доставка с'));
        $this->addColumn('bid', 'delivery_to', $this->date()->comment('Доставка до'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'executor_id');
        $this->dropColumn('bid', 'type_id');
        $this->dropColumn('bid', 'subgroup_id');
        $this->dropColumn('bid', 'delivery_from');
        $this->dropColumn('bid', 'delivery_to');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_142337_add_column_to_bid cannot be reverted.\n";

        return false;
    }
    */
}
