<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%invoice}}`.
 */
class m201226_111344_add_new_columns_to_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'sum_not_payed', $this->float()->comment('Не оплачено'));
        $this->addColumn('invoice', 'delivery', $this->boolean()->defaultValue(false)->comment('Доставка'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('invoice', 'sum_not_payed');
        $this->dropColumn('invoice', 'delivery');
    }
}
