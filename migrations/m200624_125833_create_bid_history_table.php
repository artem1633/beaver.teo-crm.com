<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bid_history`.
 */
class m200624_125833_create_bid_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bid_history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'bid_id' => $this->integer()->comment('Id Заявки'),
            'history_comment' => $this->text()->comment('Содержание действия'),
            'created_at' => $this->dateTime(),
        ]);
        $this->createIndex(
            'idx-bid_history-user_id',
            'bid_history',
            'user_id'
        );
        $this->createIndex(
            'idx-bid_history-bid_id',
            'bid_history',
            'bid_id'
        );
        $this->addForeignKey(
            'fk-bid_history-bid_id',
            'bid_history',
            'bid_id',
            'bid',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-bid_history-user_id',
            'bid_history',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bid_history-user_id',
            'bid_history'
        );

        $this->dropForeignKey(
            'fk-bid_history-bid_id',
            'bid_history'
        );
        $this->dropIndex(
            'idx-bid_history-bid_id',
            'bid_history'
        );

        $this->dropIndex(
            'idx-bid_history-user_id',
            'bid_history'
        );
        $this->dropTable('bid_history');
    }
}
