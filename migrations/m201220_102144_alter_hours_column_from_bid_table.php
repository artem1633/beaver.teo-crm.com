<?php

use yii\db\Migration;

/**
 * Class m201220_102744_alter_hours_column_from_bid_table
 */
class m201220_102144_alter_hours_column_from_bid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('bid', 'hours', $this->float()->comment('Количество часов'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('bid', 'hours', $this->integer()->comment('Количество часов'));
    }
}
