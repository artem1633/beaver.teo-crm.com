<?php

use yii\db\Migration;

/**
 * Class m200818_005546_add_bid_column
 */
class m200818_005546_add_bid_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'sn_num', $this->integer()->comment('S/N погрузчика'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'sn_num');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200818_005546_add_bid_column cannot be reverted.\n";

        return false;
    }
    */
}
