<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technc_type_subgroup`.
 */
class m200603_015408_create_technic_type_subgroup_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('technic_type_subgroup', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('назване подгрупппы'),
            'technic_type_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-technic_type_subgroup-technic_type_id',
            'technic_type_subgroup',
            'technic_type_id'
        );


        $this->addForeignKey(
            'fk-technic_type_subgroup-technic_type_id',
            'technic_type_subgroup',
            'technic_type_id',
            'technic_type',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-technic_type_subgroup-technic_type_id',
            'technic_type_subgroup'
        );

        $this->dropIndex(
            'idx-technic_type_subgroup-technic_type_id',
            'technic_type_subgroup'
        );
        $this->dropTable('technic_type_subgroup');
    }
}
