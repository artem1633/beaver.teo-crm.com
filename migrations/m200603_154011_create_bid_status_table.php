<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bid_status`.
 */
class m200603_154011_create_bid_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bid_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('цвет')
        ]);

        $this->insert('bid_status', [
            'name' => 'Создана',
        ]);
        $this->insert('bid_status', [
            'name' => 'В работе',
        ]);
        $this->insert('bid_status', [
            'name' => 'Закрыта',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bid_status');
    }
}
