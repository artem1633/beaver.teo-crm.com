<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%bid_status}}`.
 */
class m200913_102343_add_style_column_to_bid_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bid_status', 'style', $this->integer()->defaultValue(0)->comment('Стиль'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bid_status', 'style');
    }
}
