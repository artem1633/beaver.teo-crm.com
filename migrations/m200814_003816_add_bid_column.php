<?php

use yii\db\Migration;

/**
 * Class m200814_003816_add_bid_column
 */
class m200814_003816_add_bid_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'customer', $this->integer()->comment('Заказчик'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'customer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200814_003816_add_bid_column cannot be reverted.\n";

        return false;
    }
    */
}
