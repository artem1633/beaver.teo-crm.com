<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_to_object`.
 */
class m200603_011214_create_company_to_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_to_object', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'object_id' => $this->integer()->comment('Объект')
        ]);
        $this->createIndex(
            'idx-company_to_object-company_id',
            'company_to_object',
            'company_id'
        );

        $this->addForeignKey(
            'fk-company_to_object-company_id',
            'company_to_object',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-company_to_object-object_id',
            'company_to_object',
            'object_id'
        );

        $this->addForeignKey(
            'fk-company_to_object-object_id',
            'company_to_object',
            'object_id',
            'objects',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-company_to_object-object_id',
            'company_to_object'
        );

        $this->dropIndex(
            'idx-company_to_object-object_id',
            'company_to_object'
        );

        $this->dropForeignKey(
            'fk-company_to_object-company_id',
            'company_to_object'
        );

        $this->dropIndex(
            'idx-company_to_object-company_id',
            'company_to_object'
        );
        $this->dropTable('company_to_object');
    }
}
