<?php

use yii\db\Migration;

/**
 * Class m200624_013420_add_bid_column
 */
class m200624_013420_add_bid_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'rent_sum', $this->integer()->comment('Сумма аренды'));
        $this->addColumn('bid', 'delivery_sum', $this->integer()->comment('Сумма доставки'));
        $this->addColumn('bid', 'doc_num', $this->string()->comment('Номер закрывающих документов'));
        $this->addColumn('bid', 'count', $this->integer()->comment('Количество'));
        $this->addColumn('bid', 'comment', $this->text()->comment('Коментарий'));
        $this->addColumn('bid', 'pay_status', $this->string()->comment('Стаутс оплаты'));
        $this->addColumn('bid', 'doc_from', $this->date()->comment('Период ЗД с'));
        $this->addColumn('bid', 'doc_to', $this->date()->comment('Период ЗД до'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'rent_sum');
        $this->dropColumn('bid', 'delivery_sum');
        $this->dropColumn('bid', 'doc_num');
        $this->dropColumn('bid', 'count');
        $this->dropColumn('bid', 'comment');
        $this->dropColumn('bid', 'pay_status');
        $this->dropColumn('bid', 'doc_from');
        $this->dropColumn('bid', 'doc_to');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200624_013420_add_bid_column cannot be reverted.\n";

        return false;
    }
    */
}
