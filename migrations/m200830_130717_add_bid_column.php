<?php

use yii\db\Migration;

/**
 * Class m200830_130717_add_bid_column
 */
class m200830_130717_add_bid_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('bid', 'contract_num', $this->integer()->comment('Номер договора'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('bid', 'contract_num');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200830_130717_add_bid_column cannot be reverted.\n";

        return false;
    }
    */
}
