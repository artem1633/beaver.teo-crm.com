<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bid`.
 */
class m200603_154027_create_bid_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bid', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название завки'),
            'technic_id' => $this->integer()->comment('Техника'),
            'objects_id' => $this->integer()->comment('Объект'),
            'status_id' => $this->integer()->comment('Статус'),
            'rental_period_begin' => $this->date()->comment('Срок аренды c'),
            'rental_period_end' => $this->date()->comment('Срок аренды по'),
            'shift' => $this->integer()->comment('Количество смен'),
            'hours' => $this->integer()->comment('Количество часов'),
            'created_at' => $this->dateTime()->comment('дата создания'),
            'created_by' => $this->integer()->comment('автор')
        ]);
        $this->createIndex(
            'idx-bid-technic_id',
            'bid',
            'technic_id'
        );

        $this->addForeignKey(
            'fk-bid-technic_id',
            'bid',
            'technic_id',
            'technic',
            'id',
            'SET NULL'
        );
        $this->createIndex(
        'idx-bid-objects_id',
        'bid',
        'objects_id'
    );

        $this->addForeignKey(
            'fk-bid-objects_id',
            'bid',
            'objects_id',
            'objects',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-bid-status_id',
            'bid',
            'status_id'
        );

        $this->addForeignKey(
            'fk-bid-status_id',
            'bid',
            'status_id',
            'bid_status',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bid-status_id',
            'bid'
        );

        $this->dropIndex(
            'idx-bid-status_id',
            'bid'
        );

        $this->dropForeignKey(
            'fk-bid-objects_id',
            'bid'
        );

        $this->dropIndex(
            'idx-bid-objects_id',
            'bid'
        );
        $this->dropForeignKey(
            'fk-bid-technic_id',
            'bid'
        );

        $this->dropIndex(
            'idx-bid-technic_id',
            'bid'
        );
        $this->dropTable('bid');
    }
}
