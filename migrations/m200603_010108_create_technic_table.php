<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technic`.
 */
class m200603_010108_create_technic_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('technic', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название Техники'),
            'model' => $this->string()->comment('Модель '),
            'characteristics' => $this->string()->comment('Техническе характеристики'),
            'equipment' => $this->string()->comment('Останстка'),
            'type_id' => $this->integer()->comment('Тип техники'),
            'subgroup_id' =>$this->integer()->comment('Подгруппа типа')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('technic');
    }
}
