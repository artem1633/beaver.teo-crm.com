<?php

use yii\db\Migration;

/**
 * Handles the creation of table `characteristic`.
 */
class m200605_144326_create_characteristic_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('characteristic', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->comment('Тип техники'),
            'subgroup_id' => $this->integer()->comment('Подгруппа'),
            'technic_id' => $this->integer()->comment('Техника'),
            'company_id' => $this->integer()->comment('Компания'),
            'file' => $this->string()->comment('Загрузка фалйла'),
            'release_year' => $this->date()->comment('Год выпуска'),
            'mileage' => $this->string()->comment('Пробег'),
            'price_before' => $this->integer()->comment('Цена до 30 дней'),
            'price_after' => $this->integer()->comment('Цена свыше 30 дней'),
            'vin' => $this->string()->comment('VIN номер'),
        ]);
        $this->createIndex(
            'idx-characteristic-company_id',
            'characteristic',
            'company_id'
        );

        $this->addForeignKey(
            'fk-characteristic-company_id',
            'characteristic',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-characteristic-type_id',
            'characteristic',
            'type_id'
        );

        $this->addForeignKey(
            'fk-characteristic-type_id',
            'characteristic',
            'type_id',
            'technic_type',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-characteristic-subgroup_id',
            'characteristic',
            'subgroup_id'
        );

        $this->addForeignKey(
            'fk-characteristic-subgroup_id',
            'characteristic',
            'subgroup_id',
            'technic_type_subgroup',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-characteristic-technic_id',
            'characteristic',
            'technic_id'
        );

        $this->addForeignKey(
            'fk-characteristic-technic_id',
            'characteristic',
            'technic_id',
            'technic',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-characteristic-technic_id',
            'characteristic'
        );
        $this->dropIndex(
            'idx-characteristic-technic_id',
            'characteristic'
        );

        $this->dropForeignKey(
            'fk-characteristic-subgroup_id',
            'characteristic'
        );

        $this->dropIndex(
            'idx-characteristic-subgroup_id',
            'characteristic'
        );
        $this->dropForeignKey(
            'fk-characteristic-type_id',
            'characteristic'
        );

        $this->dropIndex(
            'idx-characteristic-type_id',
            'characteristic'
        );
        $this->dropForeignKey(
            'fk-characteristic-company_id',
            'characteristic'
        );

        $this->dropIndex(
            'idx-characteristic-company_id',
            'characteristic'
        );


        $this->dropTable('characteristic');
    }
}
