<?php

use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Characteristic */
?>
<div class="characteristic-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type_id',
                'value' => ArrayHelper::getValue($model->getType()->one(),'name')
            ],
            [
                'attribute' => 'subgroup_id',
                'value' => ArrayHelper::getValue($model->getSubgroup()->one(),'name')
            ],
            [
                'attribute' => 'technic_id',
                'value' => ArrayHelper::getValue($model->getTechnic()->one(),'name')
            ],

            'file',
            'release_year',
            'mileage',
            'price_before',
            'price_after',
            'vin',
        ],
    ]) ?>

</div>
