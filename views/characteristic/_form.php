<?php

use app\models\Technic;
use app\models\TechnicType;
use app\models\TechnicTypeSubgroup;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Characteristic */
/* @var $form yii\widgets\ActiveForm */


$dates = [];

for ($i = 1900; $i <= intval(date('Y')); $i++)
{
    $dates[$i] = $i;
}

$dates = array_reverse($dates);
$dates = array_combine($dates, $dates);

?>

<div class="characteristic-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TechnicType::find()->all(), 'id', 'name'),
                'language' => 'ru',
                'options' => ['placeholder' => 'выберите тип ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],'pluginEvents' => [
                    "change:select" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/characteristic/search-kind?q='+$(this).val(),
                                success: function(response){

                                    var data = '';

                                     $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                }
                            });
                        }",
                    "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/characteristic/search-kind?q='+$(this).val(),
                                success: function(response){

                                    var data = '';
                                    
                                    var currentValue = $('#characteristic-type_id').val();

                                    $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                    $('#characteristic-subgroup_id').html(data);
                                    
                                    $('#characteristic-subgroup_id').val(currentValue);
                                }
                            });   
                        }",
                ],
            ]);
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'subgroup_id')->widget(Select2::classname(), [
                'data' => $model->type_id ? ArrayHelper::map(TechnicTypeSubgroup::find()->where(['technic_type_id' => $model->type_id])->all(), 'id', 'name') : [],
                'language' => 'ru',
                'options' => ['placeholder' => 'выберите подгруппу ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],'pluginEvents' => [
                    "change:select" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/characteristic/search-tech?q='+$(this).val(),
                                success: function(response){

                                    var data = '';

                                     $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                }
                            });
                        }",
                    "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/characteristic/search-tech?q='+$(this).val(),
                                success: function(response){

                                    var data = '';
                                    
                                    var currentValue = $('#characteristic-subgroup_id').val();

                                    $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                    $('#characteristic-technic_id').html(data);
                                    
                                    $('#characteristic-technic_id').val(currentValue);
                                }
                            });   
                        }",
                ],
            ]);
            ?>

        </div>
    </div>

    <?= $form->field($model, 'technic_id')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(Technic::find()->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'выберите технику ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>


    <?= $form->field($model, 'release_year')->dropDownList($dates) ?>

    <?= $form->field($model, 'vin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mileage')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'price_before')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'price_after')->textInput() ?>
        </div>
    </div>




    <?= $form->field($model, 'upload')->fileInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
