<?php

use app\models\Company;
use app\models\Technic;
use app\models\TechnicType;
use app\models\TechnicTypeSubgroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(TechnicType::find()->where(['id' => $data->type_id])->all(), 'name');
            return implode('',$status);
            },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'subgroup_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(TechnicTypeSubgroup::find()->where(['id' => $data->subgroup_id])->all(), 'name');
            return implode('',$status);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'technic_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(Technic::find()->where(['id' => $data->technic_id])->all(), 'name');
            return implode('',$status);
        },
    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'file',
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'release_year',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'mileage',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'price_before',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'price_after',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vin',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(Company::find()->where(['id' => $data->company_id])->all(), 'name');
            return implode('',$status);
        },
        'visible' => Yii::$app->user->identity->isSuperAdmin()
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   