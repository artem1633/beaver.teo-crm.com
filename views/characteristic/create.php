<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Characteristic */

?>
<div class="characteristic-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
