<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdminFiles */
/* @var $form ActiveForm */

$this->title = "Файлы администратора";

?>
<div class="admin-files-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Файлы</h4>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'contract_creator')->fileInput() ?>
                </div>
                <div class="col-md-10">
                    <?php if($model->contract_creator != null): ?>
                        <?= Html::a("Скачать договор", '/'.$model->contract_creator, ['download' => 'Договор создателя', 'style' => 'display: block; margin-top: 22px;']) ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'contract_winner')->fileInput() ?>
                </div>
                <div class="col-md-10">
                    <?php if($model->contract_winner != null): ?>
                        <?= Html::a("Скачать договор", '/'.$model->contract_winner, ['download' => 'Договор победителя', 'style' => 'display: block; margin-top: 22px;']) ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div><!-- admin-files-index -->
