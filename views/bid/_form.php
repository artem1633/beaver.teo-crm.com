<?php

use app\models\BidStatus;
use app\models\Company;
use app\models\Objects;
use app\models\Technic;
use app\models\TechnicType;
use app\models\TechnicTypeSubgroup;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bid */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */

$data = Objects::find()->all();
$selectData = [];
foreach($data as $item){
    $selectData[$item->id] = $item->name." ($item->address)";
}
//$dataCompany = Objects::find()->where(['company_id' => Yii::$app->user->identity->company_id] )->all();
if(\Yii::$app->user->identity->isSuperAdmin()){
    $dataCompany = Objects::find()->all();
} else {
    $dataCompany = Objects::find()->andWhere(['company_id' => Yii::$app->user->identity->company_id])->all();
}
$selectDataCompany = [];
foreach($dataCompany as $item){
    $selectDataCompany[$item->id] = $item->name." ($item->address)";
}

if($model->status_id == 3){
    $model->isStatusClosed = true;
}

if($model->rental_period_begin != null && $model->rental_period_begin != null){
    $model->dates = "{$model->rental_period_begin} - {$model->rental_period_end}";
}


$executors = [];

if($model->technic_id){
    $companiesPks = ArrayHelper::getColumn(\app\models\Characteristic::find()->where(['technic_id' => $model->technic_id])->all(), 'company_id');
    $executors = Company::find()->where(['id' => $companiesPks, 'type' => 1])->all();
}


?>

<style>
    .picker__holder {
        overflow: hidden;
    }

    .modal-dialog {
        overflow: hidden;
    }
</style>

<div class="bid-form">

    <?php

        $formUrl = ["bid/{$action}"];

        $formUrl = ArrayHelper::merge($formUrl, Yii::$app->request->queryParams);

    ?>


    <?php $form = ActiveForm::begin(['id' => 'frm', 'action' => $formUrl]); ?>


   <div class="row">
       <div class="col-md-3">
           <?= $form->field($model, 'type_id')->widget(Select2::classname(), [
               'data' => ArrayHelper::map(TechnicType::find()->all(), 'id', 'name'),
               'language' => 'ru',
               'options' => ['placeholder' => 'выберите тип ...'],
               'pluginOptions' => [
                   'allowClear' => true
               ],'pluginEvents' => [
                   "change:select" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-kind?q='+$(this).val(),
                                success: function(response){

                                    var data = '';

                                     $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                }
                            });
                        }",
                   "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-kind?q='+$(this).val(),
                                success: function(response){

                                    var data = '';
                                    
                                    var currentValue = $('#bid-type_id').val();

                                    $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                    $('#bid-subgroup_id').html(data);
                                    
                                    $('#bid-subgroup_id').val(currentValue);
                                }
                            });   
                        }",
               ],
           ]);
           ?>
       </div>
       <div class="col-md-3">
           <?= $form->field($model, 'subgroup_id')->widget(Select2::classname(), [
               'data' => $model->type_id ? ArrayHelper::map(TechnicTypeSubgroup::find()->where(['technic_type_id' => $model->type_id])->all(), 'id', 'name') : [],
               'language' => 'ru',
               'options' => ['placeholder' => 'выберите подгруппу ...'],
               'pluginOptions' => [
                   'allowClear' => true
               ],'pluginEvents' => [
                   "change:select" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-tech?q='+$(this).val(),
                                success: function(response){
                                    var data = '';

                                     $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                }
                            });
                        }",
                   "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-tech?q='+$(this).val(),
                                success: function(response){

                                    var data = '';
                                    
                                    var currentValue = $('#bid-subgroup_id').val();

                                    $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                    $('#bid-technic_id').html(data);
                                    
                                    $('#bid-technic_id').val(currentValue);
                                }
                            });   
                        }",
               ],
           ]);
           ?>

       </div>
       <div class="col-md-3">
           <div class="row">
               <div class="col-md-10">
                   <?= $form->field($model, 'technic_id')->widget(Select2::classname(), [
                       'data' =>  ArrayHelper::map(Technic::find()->all(), 'id', 'name'),
                       'language' => 'ru',
                       'options' => ['placeholder' => 'выберите технику ...'],
                       'pluginOptions' => [
                           'allowClear' => true
                       ],
                       'pluginEvents' => [
                           'change' => 'function(event){
                                if($(this).val() != null){
                                    $.get("/technic/view-ajax?id="+$(this).val(), function(response){
                                        var url = "/"+response.characteristics;
                                        
                                        $("#technic-info-btn").attr("href", url);
                                        $("#bid-rental_period_begin").trigger("change");
                                    });    
                                } else {
                                
                                }                   
                           }',
                       ],
                   ]);
                   ?>
               </div>
               <div class="col-md-2">
                   <?= Html::a('<i class="fa fa-question"></i>', '#', ['id' => 'technic-info-btn', 'class' => 'btn btn-default', 'style' => 'margin-top: 22px;', 'title' => 'Посмотреть характеристики', 'target' => '_blank']) ?>
               </div>
           </div>
       </div>
       <div class="col-md-3 hidden">
           <?= $form->field($model, 'sn_num')->textInput() ?>
       </div>
</div>






    <div class="row">
        <div class="col-md-6">
            <?php if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER): ?>
                <div style="width: 48%; display: inline-block;">
                    <?= $form->field($model, 'rental_period_begin')->textInput(['readonly' => true])->label('Срок аренды') ?>
                </div>
                <div style="width: 48%; display: inline-block;">
                    <?= $form->field($model, 'rental_period_end')->textInput(['style' => 'margin-top: 22px;', 'readonly' => true])->label(false) ?>
                </div>
            <?php else: ?>
                <div style="width: 48%; display: inline-block;">
                    <?= $form->field($model, 'rental_period_begin')->input('date')->label('Срок аренды') ?>
                </div>
                <div style="width: 48%; display: inline-block;">
                    <?= $form->field($model, 'rental_period_end')->input('date', ['style' => 'margin-top: 22px;'])->label(false) ?>
                </div>
            <?php endif; ?>

            <p><b data-label="invoice"></b></p>

        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-10">
                    <?php if(Yii::$app->user->identity->isSuperAdmin() == false): ?>
                        <?= $form->field($model, 'objects_id')->widget(Select2::classname(), [
                            'data' =>  $selectDataCompany,
                            'language' => 'ru',
                            'options' => ['placeholder' => 'выберите объект ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    <?php endif; ?>
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <?= $form->field($model, 'objects_id')->widget(Select2::classname(), [

                            'data' =>  $selectData,
                            'language' => 'ru',
                            'options' => ['placeholder' => 'выберите объект ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-2">
                    <?= Html::a("<i class='fa fa-plus'></i>", $action == 'update' ? ['bid/create-object', 'id' => $model->id] : ['bid/create-object'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 22px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "/bid/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1">
            <div class="form-group" style="margin-top: 25px;">
                <label for="">Сутки</label>
                <?= Html::checkbox('24hours', $model->hours == 1 ? true : false, ['id' => '24hours-checkbox']) ?>
            </div>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'hours')->textInput() ?>
        </div>
        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'rent_sum')->textInput()->label('Ставка аренды заказчика') ?>
                    <?= $form->field($model, 'delivery_sum')->textInput()->label('Ставка доставки заказчика') ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'provider_rent_sum')->textInput()->label('Ставка аренды поставщика') ?>
                    <?= $form->field($model, 'provider_delivery_sum')->textInput()->label('Ставка доставки поставщика') ?>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                </div>
            </div>
        <?php else: ?>
            <div>
                <div class="col-md-2">
                    <?= $form->field($model, (Yii::$app->user->identity->company->type == Company::TYPE_BUYER ? 'rent_sum' : 'provider_rent_sum'))->textInput(['disabled' => true]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, (Yii::$app->user->identity->company->type == Company::TYPE_BUYER ? 'delivery_sum' : 'provider_delivery_sum'))->textInput(['disabled' => true]) ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="hidden">
            <div class="col-md-2">
                <?= $form->field($model, 'count')->dropDownList([
                    1  => 1,
                    2  => 2,
                    3  => 3,
                    4  => 4,
                    5  => 5,
                    6  => 6,
                    7  => 7,
                    8  => 8,
                    9  => 9,
                    10  => 10,
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'pay_status')->dropDownList([
                    'запрошено'  => 'запрошено',
                    'выставленно'  => 'выставленно',
                    'частично оплачено' => 'частчно оплачено',
                    'оплачено' => 'оплачено',
                    'оплачено и закрыто' => 'оплачено и закрыто',
                ]) ?>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-3">
            <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <?= $form->field($model, 'status_id')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(BidStatus::find()->all(), 'id', 'name'),
                    'language' => 'ru',
                    'options' => ['placeholder' => 'выберите статус ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <?= $form->field($model, 'executor_id')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map($executors, 'id', 'name'),
                    'language' => 'ru',
                    'options' => ['placeholder' => 'выберите исполнителя ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <?= $form->field($model, 'customer')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(Company::find()->where(['type' => 0])->all(), 'id', 'name'),
                    'language' => 'ru',
                    'options' => ['placeholder' => 'выберите заказчика ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 hidden">
                        <?= $form->field($model, 'executor_contract')->textInput() ?>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                'id' => 'my_id',
                'min' => 0,
                'columns' => [
                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden'
                        ]
                    ],
                    [
                        'name' => 'name',
                        'title' => 'Название',
                    ],
                    [
                        'name' => 'file_new',
                        'title' => 'Файл',
                        'type'  => 'fileInput',
                        'options' => [
                            'pluginOptions' => [
                                'initialPreview'=>[
                                    //add url here from current attribute
                                ],
                                'showPreview' => false,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                            ]
                        ]
                    ],

                ],
            ])->label(false) ?>
        </div>
    </div>

    <?php if(\Yii::$app->user->identity->isSuperAdmin() || \Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER): ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'comment_diller')->textarea(['rows' => 6])->label(\Yii::$app->user->identity->isSuperAdmin() ? 'Комментарий поставщика' : 'Комментарий') ?>
            </div>
        </div>

    <?php endif; ?>

    <?php if(\Yii::$app->user->identity->isSuperAdmin() || \Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER): ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'comment_client')->textarea(['rows' => 6])->label(\Yii::$app->user->identity->isSuperAdmin() ? 'Комментарий клиента' : 'Комментарий') ?>
            </div>
        </div>

    <?php endif; ?>

    <?php if(\Yii::$app->user->identity->isSuperAdmin()): ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
            </div>
        </div>

    <?php endif; ?>



    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'isStatusClosed')->checkbox()?>
        </div>
    </div>












  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

$script = <<< JS
    $('[name="24hours"]').change(function(){
        if($(this).is(':checked')){
            $("#bid-hours").val(24);
            $("#bid-hours").attr('disabled', true);
        } else {
            $("#bid-hours").val(0);
            $("#bid-hours").attr('disabled', false);
}    });

$('#bid-technic_id').change(function(){
    $.get('/bid/get-executors-by-technic?id='+$(this).val(), function(response){
        $('#bid-executor_id').html(response);
        $('#bid-executor_id').trigger('change');
    });
});


$('#bid-rental_period_begin, #bid-rental_period_end').change(function(){

    $.get("/characteristic/view-ajax?id="+$('#bid-type_id').val(), function(response){
        var priceBefore = response.price_before;
        var priceAfter = response.price_after;
        
        var start = $('#bid-rental_period_begin').val();    
        var end = $('#bid-rental_period_end').val();    
        
        start = start.split('.').reverse().join('-');
        end = end.split('.').reverse().join('-');
        
        console.log(start);
        console.log(end);
        
        var days = getDaysBetweenDates(start, end);
        
        console.log(priceBefore);
        console.log(priceAfter);
        console.log(days);
        
        if(days < 30){
            $('#bid-provider_rent_sum').val(priceBefore);
            var rentSum = priceBefore;
        } else {
            $('#bid-provider_rent_sum').val(priceAfter);
            var rentSum = priceAfter;
        }
        
        $.get("/bid/get-form-data?startDate="+start+"&endDate="+end+'&provider_rent_sum='+rentSum, function(response){
            $("[data-label='invoice']").text("Кол-во смен: "+response.not_payed);
        });
    });    
});




function getDaysBetweenDates(d0, d1) {

  var msPerDay = 8.64e7;

  // Copy dates so don't mess them up
  var x0 = new Date(d0);
  var x1 = new Date(d1);

  // Set to noon - avoid DST errors
  x0.setHours(12,0,0);
  x1.setHours(12,0,0);

  // Round to remove daylight saving errors
  return Math.round( (x1 - x0) / msPerDay );
}


JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
