<?php

use app\models\BidStatus;
use app\models\Company;
use app\models\Objects;
use app\models\Technic;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;



return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
         [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'id',
             'width' => '3%',
             'hAlign' => GridView::ALIGN_CENTER,
         ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'technic_id',
        'content' => function($data){
            $contract = ArrayHelper::getColumn(Technic::find()->where(['id' => $data->technic_id])->all(), 'name');
            return implode('',$contract);

        },
        'filter' => ArrayHelper::map(Technic::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'objects_id',
        'content' => function($data){
            $contract = ArrayHelper::getColumn(Objects::find()->where(['id' => $data->objects_id])->all(), 'name');
            return implode('',$contract);
            },
        'filter' => \app\models\Bid::getObjectsList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'attribute' => 'status_id',
        'value' => function ($model, $key, $index, $widget) {
            $statusName = implode('',ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'name'));
            $statusColor = implode('', ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'color'));
            return "<div style='background-color: {$statusColor};color: black; text-align: center;'> <span>{$statusName}</span></div>";

        },
        'width' => '6%',
        'filter' => ArrayHelper::map(BidStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => 'dropdown',
        'format' => 'raw',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rental_period_begin',
        'format' => ['date', 'php:d.m.Y'],
        'label' => 'Аренда С',
        'width' => '6%',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'rental_period_end',
         'format' => ['date', 'php:d.m.Y'],
         'label' => 'Аренда По',
         'width' => '6%',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'shift',
         'label' => 'Смены',
         'width' => '4%',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'hours',
         'label' => 'Часов',
         'content' => function($data){
            $hours = $data->hours;
            if ($hours == 1){
                return 'Сутки';
            }else{
                return $hours;
            }
         },
         'width' => '4%',
     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'delivery_from',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sn_num',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contract_num',
        'visible' => Yii::$app->user->identity->company->type != 0 || Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer',
        'content' => function($data){
//            $company = ArrayHelper::getColumn(Company::find()->where(['id' => $data->created_by])->all(), 'name');
//            return implode('',$company);
            $companies = ArrayHelper::map(Company::find()->where(['type' => 0])->all(), 'id', 'name');

            if(isset($companies[$data->customer])){
               return $companies[$data->customer];
            }

            return null;
        },
        'filter' => ArrayHelper::map(Company::find()->where(['type' => 0])->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function($data){
            $company = ArrayHelper::getColumn(Company::find()->where(['id' => $data->executor_id])->all(), 'name');
            if(count($company) > 0){
                return implode('',$company);
            }

        },
        'filter' => ArrayHelper::map(Company::find()->where(['type' => 1])->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by_user_id',
        'content' => function($data){
//            $company = ArrayHelper::getColumn(Company::find()->where(['id' => $data->created_by])->all(), 'name');
            $user = \app\models\User::find()->where(['id' => $data->created_by_user_id])->one();

            if($user){
                return $user->name;
            }

        },
        'filter' => ArrayHelper::map(Company::find()->where(['type' => 1])->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'label' => 'Создана',
        'format' => ['date', 'php:H:i d.m.Y'],
        'width' => '6%',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {delete} {clone}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['bid/'.$action,'id'=>$key]);
        },
        'buttons' => [
            'clone' => function ($url,$model,$key) {
                return Html::a( '<i class="fa fa-files-o" style="font-size: 16px;"title="Клонировать"data-toggle="tooltip"></i>', ['bid/clone', 'id' => $model->id], ['role' => 'modal-remote']);
            },
            'update' => function ($url, $model, $key) {
//                if((Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER && ArrayHelper::getValue($model, 'status.name') != "В работе") && Yii::$app->user->identity->company->type != \app\models\Company::TYPE_PROVIDER){
//                if(ArrayHelper::getValue($model, 'status.name') != "В работе"){
                    return Html::a( '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['bid/update', 'id' => $model->id], ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
//                }
            },
        ],
        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Удаление',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Вы уверены?',
//            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
//    ],

];   