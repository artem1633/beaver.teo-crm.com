<?php


use app\models\Timesheet;
use app\models\User;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $timesheet_model app\models\Timesheet */
/* @var $this yii\web\View */
/* @var $model app\models\Bid */
$bid_id = $model->id;


$onchange = <<<JS
    var elem = $(this);
    var row = elem.parent('div').parent('td').parent('tr');



    var name = elem.attr('data-name');
    var value = elem.val();
    var id = row.find('.ls-id').val();

     $.post(
        '/bid/save-structure',
        {id:id, name:name, value:value},
        function(res){
            console.log(res);
        }
    );
    return true;

JS;



$detailViewAttributes = [];

if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
    $detailViewAttributes = [
        [
            'attribute' => 'rent_sum',
            'label' => 'Сумма аренеды клиента'
        ],
        [
            'attribute' => 'delivery_sum',
            'label' => 'Сумма доставки клиента'
        ],
        [
            'attribute' => 'provider_rent_sum',
            'label' => 'Сумма аренеды поставщика'
        ],
        [
            'attribute' => 'provider_delivery_sum',
            'label' => 'Сумма доставки поставщика'
        ],
        'count',
        'comment' ,
        'pay_status',
//        'sn_num'
    ];
} else {
    $detailViewAttributes = [
        (Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER ? 'rent_sum' : 'provider_rent_sum'),
        (Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER ? 'delivery_sum' : 'provider_delivery_sum'),
        'count',
        'comment' ,
        'pay_status',
//        'sn_num'
    ];
}

?>

<style>
    .table-graph .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        border-color: #e2e7eb;
        padding: 6px 4px !important;
        background: #fff;
    }

    .table-graph .help-block {
        display: none;
    }

    @media screen and (max-width: 1684px)
    {
        .table-graph td {
            font-size: 10px;
        }

        .table-graph td input {
            height: 25px;
            padding: 3px 7px;
        }

        .table-graph>tbody>tr>td, .table-graph>tbody>tr>th, .table-graph>tfoot>tr>td, .table-graph>tfoot>tr>th, .table-graph>thead>tr>td, .table-graph>thead>tr>th {
            padding: 8px 5px;
        }
    }


    @media screen and (max-width: 1440px)
    {
        .table-graph td {
            font-size: 9px;
        }

        .table-graph td input {
            height: 20px;
            padding: 1px 4px;
        }

        .table-graph>tbody>tr>td, .table-graph>tbody>tr>th, .table-graph>tfoot>tr>td, .table-graph>tfoot>tr>th, .table-graph>thead>tr>td, .table-graph>thead>tr>th {
            padding: 5px 2px;
        }
    }

</style>

<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Назад', ['bid/index'], ['class' => 'btn btn-warning']) ?>
        </p>
    </div>
</div>

<?php Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]); ?>
<div class="bid-view">

    <div class="row">


        <div class="col-md-7">
            <div class="row">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Информация</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <?php if((Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER || Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER)): ?>
                                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['bid/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'id',
                                        [
                                            'attribute' => 'technic_id',
                                            'value' => ArrayHelper::getValue($model->getTechnic()->one(),'name')
                                        ],
                                        [
                                            'attribute' => 'objects_id',
                                            'value' => ArrayHelper::getValue($model->getObjects()->one(),'address')
                                        ],
                                        [
                                            'attribute' => 'status_id',
                                            'value' => ArrayHelper::getValue($model->getStatus()->one(),'name')
                                        ],
                                        [
                                            'attribute' => 'rental_period_begin',
                                            'format' => ['date', 'php:d.m.Y'],
                                        ],
                                        [
                                            'attribute' => 'rental_period_end',
                                            'format' => ['date', 'php:d.m.Y'],
                                        ],
                                        'shift',
                                        'hours',
                                        [
                                            'attribute' => 'created_at',
                                            'format' => ['date', 'php:d.m.Y'],
                                        ],
                                        [
                                            'attribute' => 'created_by',
                                            'value' => function($model){
                                            	// $user = \app\models\User::findOne($model->created_by);
                                            	// if($user){
                                            		$user = \app\models\User::findOne($model->created_by_user_id);

                                            		if($user){
                                            			return $user->email;
                                            		}
                                            	// }
                                            }
                                        ],
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => $detailViewAttributes,
                                ]) ?>
                            </div>

                        </div>
                        <div class="row">
                            <?php if(Yii::$app->user->identity->company->type != \app\models\Company::TYPE_BUYER || Yii::$app->user->identity->isSuperAdmin()): ?>
                                <div class="<?=(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_PROVIDER && Yii::$app->user->identity->isSuperAdmin() == false) ? 'col-md-12' : 'col-md-6'?>">
                                    <div class="panel-heading">
                                        <h4 class="panel-title" style="background: #242a30;padding: 10px 15px;color: #fff;border: none;">Информация о исполнителе</h4>
                                    </div>
                                    <div class="panel-body">
                                        <?= ($executor_model ? DetailView::widget([
                                            'model' => $executor_model,
                                            'attributes' => [
                                                'name',
                                                'address',
                                                'email',
                                                'phone',
                                                'site'
                                            ]
                                        ]) : '')
                                        ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(Yii::$app->user->identity->company->type != \app\models\Company::TYPE_PROVIDER || Yii::$app->user->identity->isSuperAdmin()): ?>
                                <div class="<?=(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER && Yii::$app->user->identity->isSuperAdmin() == false) ? 'col-md-12' : 'col-md-6'?>">
                                    <div class="panel-heading">
                                        <h4 class="panel-title" style="background: #242a30;padding: 10px 15px;color: #fff;border: none;">Информация о заказчике</h4>
                                    </div>
                                    <div class="panel-body">
                                        <?= ( $customer_model ? DetailView::widget([
                                            'model' => $customer_model,
                                            'attributes' => [
                                                'name',
                                                'address',
                                                'email',
                                                'phone',
                                                'site'
                                            ]
                                        ]) : '')
                                        ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <div class="panel panel-inverse panel-bid-structure">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Граффик часов заказчика
                                </h4>
                                <?php Pjax::begin(['id' => 'pjax-graph-container1']) ?>
                                <span>Количество дней: <?=$model->getDaysCount(true)?></span>&nbsp&nbsp&nbsp&nbsp
                                <span>Колчество часов: <?=$model->getHoursCount()?></span>&nbsp&nbsp&nbsp&nbsp
                                <span>Часов переработки: <?=$model->getRecast()?></span>
                                <?php Pjax::end() ?>
                                <div class="panel-heading-btn" style="margin-top: -20px;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['timesheet/create', 'bid_id' => $bid_id, 'containerPjaxReload'=>'#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-graph">
                                        <?php
                                        $counter=0;
                                        $weeksCount = ceil ($model->getDaysCount() / 7) + 1;
                                        for($i = 0; $i < $weeksCount; $i++): ?>
                                            <thead>
                                            <tr>
                                                <?php
                                                if ($counter >= count($timesheet_model)-2){
//                                                break;
                                                }
                                                if(isset($timesheet_model[$counter])){
                                                    $date1 = $timesheet_model[$counter];
                                                    $weekDayNum = strtotime($date1->date);
                                                    $weekDayNum = date("w", $weekDayNum);
                                                    if($weekDayNum == 0){
                                                        $weekDayNum = 7;
                                                    }
                                                    $c1 = 1;

                                                    if ($weekDayNum != 0) {

                                                        for($i1 = 2; $i1 <= $weekDayNum; $i1++){
                                                            ?>
                                                            <th style="width: 80px;background-color: gray;"></th>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>

                                                <?php $secondCounter = $counter; ?>
                                                <?php for($i2 = $weekDayNum; $i2 <= 7; $i2++): ?>

                                                    <?php
                                                    if(isset($timesheet_model[$counter])){
                                                        $date2 = $timesheet_model[$counter];
                                                        $weekend = $model->isWeekend($date2->date);

                                                        $counter ++;

                                                        if($date2 != null){
                                                            echo ' <th style="width: 80px;background-color: '.($weekend?'#fff':'#33adff').'">'.date('d.m.Y',strtotime($date2->date)).'</th>';
                                                        } else {
                                                            echo "<th style=\"width: 80px;background-color: gray;\"></th>";
                                                        }
                                                    } else {
                                                        echo "<th style=\"width: 80px;background-color: gray;\"></th>";
                                                    }


                                                    ?>


                                                <?php endfor; ?>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            <tr>
                                                <?php if(isset($timesheet_model[$secondCounter])): ?>
                                                    <?php
                                                    if ($weekDayNum != 0) {

                                                        for($i1 = 2; $i1 <= $weekDayNum; $i1++){?>

                                                            <th>

                                                            </th>



                                                            <?php
                                                        }
                                                    }
                                                    for($i2 = $weekDayNum; $i2 <= 7; $i2++): ?>
                                                        <?php

                                                        if(isset($timesheet_model[$secondCounter])){
                                                            $date2 = $timesheet_model[$secondCounter];
                                                        } else {
                                                            $date2 = null;
                                                        }

                                                        $form = ActiveForm::begin(); ?>
                                                        <td>
                                                        <?php if($date2 != null): ?>
                                                            <?= $form->field($date2, 'hours')->textInput(['style' => 'width: 80px;', 'data-ts-id' => $date2->id, 'data-type' => '1']) ?></td>
                                                        <?php endif; ?>
                                                        <?php ActiveForm::end(); ?>
                                                        <?php $secondCounter++; ?>
                                                    <?php endfor; ?>
                                                <?php endif; ?>

                                            </tr>

                                            </tbody>












                                        <?php endfor; ?>

                                    </table>
                                </div>

                            </div>
                        </div>
                        <div class="panel panel-inverse panel-bid-structure">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Граффик часов
                                </h4>
                                <?php Pjax::begin(['id' => 'pjax-graph-container2']) ?>
                                <span>Количество дней: <?=$model->getDaysCount(true, true)?></span>&nbsp&nbsp&nbsp&nbsp
                                <span>Колчество часов: <?=$model->getHoursCount()?></span>&nbsp&nbsp&nbsp&nbsp
                                <span>Часов переработки: <?=$model->getRecast(true)?></span>
                                <?php Pjax::end() ?>
                                <div class="panel-heading-btn" style="margin-top: -20px;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['timesheet/create', 'bid_id' => $bid_id, 'containerPjaxReload'=>'#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-graph">
                                        <?php
                                        $counter=0;
                                        $weeksCount = ceil ($model->getDaysCount() / 7) + 1;
                                        for($i = 0; $i < $weeksCount; $i++): ?>
                                            <thead>
                                            <tr>
                                                <?php
                                                if ($counter >= count($timesheet_model)-2){
//                                                break;
                                                }
                                                if(isset($timesheet_model[$counter])){
                                                    $date1 = $timesheet_model[$counter];
                                                    $weekDayNum = strtotime($date1->date);
                                                    $weekDayNum = date("w", $weekDayNum);
                                                    if($weekDayNum == 0){
                                                        $weekDayNum = 7;
                                                    }
                                                    $c1 = 1;

                                                    if ($weekDayNum != 0) {

                                                        for($i1 = 2; $i1 <= $weekDayNum; $i1++){
                                                            ?>
                                                            <th style="width: 80px;background-color: gray;"></th>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>

                                                <?php $secondCounter = $counter; ?>
                                                <?php for($i2 = $weekDayNum; $i2 <= 7; $i2++): ?>

                                                    <?php
                                                    if(isset($timesheet_model[$counter])){
                                                        $date2 = $timesheet_model[$counter];
                                                        $weekend = $model->isWeekend($date2->date);

                                                        $counter ++;

                                                        if($date2 != null){
                                                            echo ' <th style="width: 80px;background-color: '.($weekend?'#fff':'#33adff').'">'.date('d.m.Y', strtotime($date2->date)).'</th>';
                                                        } else {
                                                            echo "<th style=\"width: 80px;background-color: gray;\"></th>";
                                                        }
                                                    } else {
                                                        echo "<th style=\"width: 80px;background-color: gray;\"></th>";
                                                    }


                                                    ?>


                                                <?php endfor; ?>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            <tr>
                                                <?php if(isset($timesheet_model[$secondCounter])): ?>
                                                    <?php
                                                    if ($weekDayNum != 0) {

                                                        for($i1 = 2; $i1 <= $weekDayNum; $i1++){?>

                                                            <th>

                                                            </th>



                                                            <?php
                                                        }
                                                    }
                                                    for($i2 = $weekDayNum; $i2 <= 7; $i2++): ?>
                                                        <?php

                                                        if(isset($timesheet_model[$secondCounter])){
                                                            $date2 = $timesheet_model[$secondCounter];
                                                        } else {
                                                            $date2 = null;
                                                        }

                                                        $form = ActiveForm::begin(); ?>
                                                        <td>
                                                        <?php if($date2 != null): ?>
                                                            <?= $form->field($date2,  'provider_hours')->textInput(['style' => 'width: 80px;', 'data-ts-id' => $date2->id, 'data-type' => '2']) ?></td>
                                                        <?php endif; ?>
                                                        <?php ActiveForm::end(); ?>
                                                        <?php $secondCounter++; ?>
                                                    <?php endfor; ?>
                                                <?php endif; ?>

                                            </tr>

                                            </tbody>












                                        <?php endfor; ?>

                                    </table>
                                </div>

                            </div>
                        </div>
                    <?php else: ?>
                        <div class="panel panel-inverse panel-bid-structure">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Граффик часов
                                </h4>
                                <?php Pjax::begin(['id' => 'pjax-graph-container2']) ?>
                                <span>Количество дней: <?=$model->getDaysCount(true, true)?></span>&nbsp&nbsp&nbsp&nbsp
                                <span>Колчество часов: <?=$model->getHoursCount()?></span>&nbsp&nbsp&nbsp&nbsp
                                <span>Часов переработки: <?=$model->getRecast()?></span>
                                <?php Pjax::end() ?>
                                <div class="panel-heading-btn" style="margin-top: -20px;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['timesheet/create', 'bid_id' => $bid_id, 'containerPjaxReload'=>'#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-graph">
                                        <?php
                                        $counter=0;
                                        $weeksCount = ceil ($model->getDaysCount() / 7) + 1;
                                        for($i = 0; $i < $weeksCount; $i++): ?>
                                            <thead>
                                            <tr>
                                                <?php
                                                if ($counter >= count($timesheet_model)-2){
//                                                break;
                                                }
                                                if(isset($timesheet_model[$counter])){
                                                    $date1 = $timesheet_model[$counter];
                                                    $weekDayNum = strtotime($date1->date);
                                                    $weekDayNum = date("w", $weekDayNum);
                                                    if($weekDayNum == 0){
                                                        $weekDayNum = 7;
                                                    }
                                                    $c1 = 1;

                                                    if ($weekDayNum != 0) {

                                                        for($i1 = 2; $i1 <= $weekDayNum; $i1++){
                                                            ?>
                                                            <th style="width: 80px;background-color: gray;"></th>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>

                                                <?php $secondCounter = $counter; ?>
                                                <?php for($i2 = $weekDayNum; $i2 <= 7; $i2++): ?>

                                                    <?php
                                                    if(isset($timesheet_model[$counter])){
                                                        $date2 = $timesheet_model[$counter];
                                                        $weekend = $model->isWeekend($date2->date);

                                                        $counter ++;

                                                        if($date2 != null){
                                                            echo ' <th style="width: 80px;background-color: '.($weekend?'#fff':'#33adff').'">'.date('d.m.Y', strtotime($date2->date)).'</th>';
                                                        } else {
                                                            echo "<th style=\"width: 80px;background-color: gray;\"></th>";
                                                        }
                                                    } else {
                                                        echo "<th style=\"width: 80px;background-color: gray;\"></th>";
                                                    }


                                                    ?>


                                                <?php endfor; ?>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            <tr>
                                                <?php if(isset($timesheet_model[$secondCounter])): ?>
                                                    <?php
                                                    if ($weekDayNum != 0) {

                                                        for($i1 = 2; $i1 <= $weekDayNum; $i1++){?>

                                                            <th>

                                                            </th>



                                                            <?php
                                                        }
                                                    }
                                                    for($i2 = $weekDayNum; $i2 <= 7; $i2++): ?>
                                                        <?php

                                                        if(isset($timesheet_model[$secondCounter])){
                                                            $date2 = $timesheet_model[$secondCounter];
                                                        } else {
                                                            $date2 = null;
                                                        }

                                                        $form = ActiveForm::begin(); ?>
                                                        <td>
                                                        <?php if($date2 != null): ?>
                                                            <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                                                                <p style="font-size: 18px;"><b class="text-success"><?=$date2->hours?></b>/<b class="text-warning"><?=$date2->provider_hours?></b></p>
                                                            <?php else: ?>
                                                                <?= $form->field($date2, Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER ? 'hours' : 'provider_hours')->textInput(['style' => 'width: 80px;', 'data-ts-id' => $date2->id]) ?></td>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        <?php ActiveForm::end(); ?>
                                                        <?php $secondCounter++; ?>
                                                    <?php endfor; ?>
                                                <?php endif; ?>

                                            </tr>

                                            </tbody>












                                        <?php endfor; ?>

                                    </table>
                                </div>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                    <div class="col-md-12">

                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Счета заказчика. Оплачено смен: <?php

                                    $payed = 0;

                                    $payedDays = 0;

                                    foreach($invoiceDataProvider->models as $invoice)
                                    {
                                        if($invoice->pay_date == false){
                                            continue;
                                        }

                                        if ($invoice->delivery){
                                            $payed = $payed + ($invoice->sum - $model->delivery_sum);
                                        } else {
                                            $payed = $payed + $invoice->sum ;
                                        }
                                    }

                                    if($model->rent_sum != null){
                                        $totalDays = $model->shift;
                                        $payedDays = round($payed / $model->rent_sum);
                                    } else {
                                        $payedDays = 0;
                                    }

                                    echo $payedDays." (".($payedDays * $model->hours)." часов)";

                                    ?>. Неоплченных смен: <?= $model->getDaysCount(true) ?> (<?=($model->getDaysCount(true) - $payedDays) * $model->hours?> часов). <?php

                                    $payedSum = 0;

                                    foreach($invoiceDataProvider->models as $invoice)
                                    {
                                        $payedSum = $payedSum + $invoice->sum;
                                    }

                                    echo "Итого оплачено: {$payedSum} руб";

                                    ?></h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['invoice/create', 'bid_id' => $bid_id, 'type' => '1', 'containerPjaxReload'=>'#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px">
                                <?= $this->render('@app/views/invoice/index', [
                                    'searchModel' => $invoiceSearchModel,
                                    'dataProvider' => $invoiceDataProvider,
                                    'bid_id' => $model->id,
                                    'ajaxModal' => false
                                ]) ?>

                            </div>
                        </div>
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Счета поставщика. Оплачено смен: <?php

                                    $payed = 0;

                                    $payedDays = 0;

                                    foreach($invoiceProviderDataProvider->models as $invoice)
                                    {
                                        if($invoice->pay_date == false){
                                            continue;
                                        }

                                        if ($invoice->delivery){
                                            $payed = $payed + ($invoice->sum - $model->provider_delivery_sum);
                                        } else {
                                            $payed = $payed + $invoice->sum;
                                        }
                                    }

                                    if($model->provider_rent_sum != null){
                                        $totalDays = $model->shift;
                                        $payedDays = round($payed / $model->provider_rent_sum);
                                    } else {
                                        $payedDays = 0;
                                    }

                                    echo $payedDays." (".($payedDays * $model->hours)." часов)";

                                    ?>. Неоплченных смен: <?=$model->getDaysCount(true, true) ?> (<?=($model->getDaysCount(true, true) - $payedDays) * $model->hours?> часов). <?php

                                    $payedSum = 0;

                                    foreach($invoiceProviderDataProvider->models as $invoice)
                                    {
                                        $payedSum = $payedSum + $invoice->sum;
                                    }

                                    echo "Итого оплачено: {$payedSum} руб";

                                    ?></h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['invoice/create', 'bid_id' => $bid_id, 'type' => '2', 'containerPjaxReload'=>'#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px">
                                <?= $this->render('@app/views/invoice/index', [
                                    'searchModel' => $invoiceProviderSearchModel,
                                    'dataProvider' => $invoiceProviderDataProvider,
                                    'bid_id' => $model->id,
                                    'ajaxModal' => false
                                ]) ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Документы заказчика</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'bid_id' => $bid_id, 'type' => 1, 'containerPjaxReload' => '#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>


                            <div class="panel-body" style="height: 500px; overflow-y: auto;height: 200px">
                                <div class="col-md-12">
                                    <?= $this->render('@app/views/bid/scan_index', [
                                        'searchModel' => $scanSearchModel,
                                        'dataProvider' => $scanDataProvider,
                                        'bid_id' => $model->id,
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Документы поставщика</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'bid_id' => $bid_id, 'type' => 2, 'containerPjaxReload' => '#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>


                            <div class="panel-body" style="height: 500px; overflow-y: auto;height: 200px">
                                <div class="col-md-12">
                                    <?= $this->render('@app/views/bid/scan_index', [
                                        'searchModel' => $scanProviderSearchModel,
                                        'dataProvider' => $scanProviderDataProvider,
                                        'bid_id' => $model->id,
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if(Yii::$app->user->identity->company->type === \app\models\Company::TYPE_BUYER): ?>
                    <div class="col-md-12">

                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Счета</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <div class="hidden">
                                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['invoice/create', 'bid_id' => $bid_id, 'type' => '1', 'containerPjaxReload'=>'#pjax-container-info-container'],
                                            ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    </div>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px">
                                <?= $this->render('@app/views/invoice/index', [
//                                    'searchModel' => $invoiceSearchModel,
//                                    'dataProvider' => $invoiceDataProvider,
                                    'searchModel' => $invoiceSearchModel,
                                    'dataProvider' => $invoiceDataProvider,
                                    'bid_id' => $model->id,
                                    'ajaxModal' => false
                                ]) ?>

                            </div>
                        </div>

                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Документы</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'bid_id' => $bid_id, 'type' => 1, 'containerPjaxReload' => '#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>


                            <div class="panel-body" style="height: 500px; overflow-y: auto;height: 200px">
                                <div class="col-md-12">
                                    <?= $this->render('@app/views/bid/scan_index', [
                                        'searchModel' => $scanSearchModel,
                                        'dataProvider' => $scanDataProvider,
                                        'bid_id' => $model->id,
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
                <?php if(Yii::$app->user->identity->company->type === \app\models\Company::TYPE_PROVIDER): ?>
                    <div class="col-md-12">

                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Счета</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['invoice/create', 'bid_id' => $bid_id, 'type' => '2', 'containerPjaxReload'=>'#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить счет','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px">
                                <?= $this->render('@app/views/invoice/index', [
                                    'searchModel' => $invoiceProviderSearchModel,
                                    'dataProvider' => $invoiceProviderDataProvider,
                                    'bid_id' => $model->id,
                                    'ajaxModal' => false
                                ]) ?>

                            </div>
                        </div>

                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">Документы</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'bid_id' => $bid_id, 'type' => 2, 'containerPjaxReload' => '#pjax-container-info-container'],
                                        ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>


                            <div class="panel-body" style="height: 500px; overflow-y: auto;height: 200px">
                                <div class="col-md-12">
                                    <?= $this->render('@app/views/bid/scan_index', [
                                        'searchModel' => $scanProviderSearchModel,
                                        'dataProvider' => $scanProviderDataProvider,
                                        'bid_id' => $model->id,
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
                <div class="col-md-12">
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <h4 class="panel-title">История изменений</h4>
                                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px">
                                <?= $this->render('@app/views/bid-history/index', [
                                    'searchModel' => $bidHistorySearchModel,
                                    'dataProvider' => $bidHistoryDataProvider,
                                    'bid_id' => $model->id,
                                ]) ?>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$csrfParam = Yii::$app->request->csrfParam;
$csrfToken = Yii::$app->request->csrfToken;

if(Yii::$app->user->identity->isSuperAdmin()){
    $script = <<< JS

$('[data-ts-id]').change(function(){
    var tsId = $(this).data('ts-id');
    
    if($(this).val() > 24){
        alert('Значение не должно быть больше 24');
        $(this).val(24);
    }
    
        var data = {
            {$csrfParam}: '{$csrfToken}',
            'tsId': tsId,
            'hours': $(this).val(),
            'type': $(this).data('type'),
        };
        
        $.ajax({
            'method': 'POST',
            'url': '/bid/update-hours-admin',
            'data': data,
            'success': function(){
                $.pjax.reload('#pjax-graph-container1');
                 setTimeout(function(){
                     $.pjax.reload('#pjax-graph-container2');
                 }, 250);
            },
        });     
   
});

JS;
} else {
    $script = <<< JS

$('[data-ts-id]').change(function(){
    var tsId = $(this).data('ts-id');
    
    if($(this).val() > 24){
        alert('Значение не должно быть больше 24');
        $(this).val(24);
    }
    
    
        var data = {
            {$csrfParam}: '{$csrfToken}',
            'tsId': tsId,
            'hours': $(this).val(),
        };
        
        $.ajax({
            'method': 'POST',
            'url': '/bid/update-hours',
            'data': data,
            'success': function(){
                $.pjax.reload('#pjax-graph-container1');
                 setTimeout(function(){
                     $.pjax.reload('#pjax-graph-container2');
                 }, 250);
            },
        });     
   
});

JS;
}

$this->registerJs($script, \yii\web\View::POS_READY);

?>


<?php Pjax::end() ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>








