<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bid */
?>
<div class="bid-update">

    <?php if(Yii::$app->user->identity->company->type != \app\models\Company::TYPE_PROVIDER): ?>
        <?= $this->render('_form', [
            'model' => $model,
            'action' => 'update'
        ]) ?>
    <?php else: ?>
        <?= $this->render('_form_provider', [
            'model' => $model,
            'action' => 'update'
        ]) ?>
    <?php endif; ?>

</div>
