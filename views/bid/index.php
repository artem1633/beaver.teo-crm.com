<?php
use app\models\BidStatus;
use app\models\Company;
use app\models\Objects;
use app\models\Technic;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BidSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Заявки";
$this->params['breadcrumbs'][] = $this->title;

$noPaginationDataProvider = clone $dataProvider;
$noPaginationDataProvider->pagination = false;

CrudAsset::register($this);
Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]);


if(Yii::$app->user->identity->isSuperAdmin()){

$columns = [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
        'width' => '3%',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'technic_id',
                        'content' => function($data){
                            $contract = ArrayHelper::getColumn(Technic::find()->where(['id' => $data->technic_id])->all(), 'name');
                            return implode('',$contract);

                        },
                        'filter' => ArrayHelper::map(Technic::find()->where(['id' => ArrayHelper::getColumn($noPaginationDataProvider->models, 'technic_id')])->asArray()->all(), 'id', 'name'),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['prompt' => ''],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'rental_period_begin',
                        'format' => ['date', 'php:d.m.Y'],
                        'label' => 'Аренда С',
                        'width' => '6%',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'rental_period_end',
                        'format' => ['date', 'php:d.m.Y'],
                        'label' => 'Аренда По',
                        'width' => '6%',
                    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'objects_id',
        'content' => function($data){
            $contract = ArrayHelper::getColumn(Objects::find()->where(['id' => $data->objects_id])->all(), 'name');
            return implode('',$contract);
        },
        'filter' => \app\models\Bid::getObjectsList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
//    [
//        'attribute' => 'status_id',
//        'value' => function ($model, $key, $index, $widget) {
//            $statusName = implode('',ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'name'));
//            $statusColor = implode('', ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'color'));
//            return "<div style='background-color: {$statusColor};color: black; text-align: center;'> <span>{$statusName}</span></div>";
//
//        },
//        'width' => '6%',
//        'filter' => ArrayHelper::map(BidStatus::find()->asArray()->all(), 'id', 'name'),
//        'filterType' => 'dropdown',
//        'format' => 'raw',
//        'hAlign' => GridView::ALIGN_CENTER,
//    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'shift',
//                        'label' => 'Смены',
//                        'width' => '4%',
//                    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'hours',
//        'label' => 'Часов',
//        'content' => function($data){
//            $hours = $data->hours;
//            if ($hours == 1){
//                return 'Сутки';
//            }else{
//                return $hours;
//            }
//        },
//        'width' => '4%',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'delivery_from',
//    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'sn_num',
//                    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'contract_num',
//                        'visible' => Yii::$app->user->identity->isSuperAdmin()
//                    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer',
        'content' => function($data){
//            $company = ArrayHelper::getColumn(Company::find()->where(['id' => $data->created_by])->all(), 'name');
//            return implode('',$company);
            $companies = ArrayHelper::map(Company::find()->where(['type' => 0])->all(), 'id', 'name');

            if(isset($companies[$data->customer])){
                return $companies[$data->customer];
            }

            return null;
        },
        'filter' => ArrayHelper::map(Company::find()->where(['id' => ArrayHelper::getColumn($noPaginationDataProvider->models, 'customer')])->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
        'visible' => Yii::$app->user->identity->isSuperAdmin()
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function($data){
            $company = ArrayHelper::getColumn(Company::find()->where(['id' => $data->executor_id])->all(), 'name');
            if(count($company) > 0){
                return implode('',$company);
            }

        },
        'filter' => ArrayHelper::map(Company::find()->where(['type' => 1])->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by_user_id',
        'content' => function($data){
            $user = \app\models\User::find()->where(['id' => $data->created_by_user_id])->one();

            if($user){

//                                $fio = [];
//
//                                if($user->last_name){
//                                    $fio[] = $user->last_name;
//                                }
//                                if($user->name){
//                                    $fio[] = $user->name;
//                                }
//                                if($user->patronymic){
//                                    $fio[] = $user->patronymic;
//                                }

//                                return implode(' ', $fio);
                return $user->email;
            }

        },
        'filter' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'email'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
        'visible' => Yii::$app->user->identity->isSuperAdmin()
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'label' => 'Создана',
        'format' => ['date', 'php:H:i d.m.Y'],
        'width' => '6%',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {delete} {clone}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['bid/'.$action,'id'=>$key]);
        },
        'buttons' => [
            'clone' => function ($url,$model,$key) {
                return Html::a( '<i class="fa fa-files-o" style="font-size: 16px;"title="Клонировать"data-toggle="tooltip"></i>', ['bid/clone', 'id' => $model->id], ['role' => 'modal-remote']);
            },
            'update' => function ($url, $model, $key) {
//                                if((Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER && ArrayHelper::getValue($model, 'status.name') != "В работе") && Yii::$app->user->identity->company->type != \app\models\Company::TYPE_PROVIDER){
//                                if((Yii::$app->user->identity->isSuperAdmin() || ArrayHelper::getValue($model, 'status.name') != "В работе")){
                return Html::a( '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['bid/update', 'id' => $model->id], ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
//                                }
            },
        ],
        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Удаление',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Вы уверены?',
//            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
//    ],

];
} else {
    if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
        $columns = [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'width' => '20px',
            ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'id',
                'width' => '3%',
                'hAlign' => GridView::ALIGN_CENTER,
            ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'technic_id',
                'content' => function($data){
                    $contract = ArrayHelper::getColumn(Technic::find()->where(['id' => $data->technic_id])->all(), 'name');
                    return implode('',$contract);

                },
                'filter' => ArrayHelper::map(Technic::find()->where(['id' => ArrayHelper::getColumn($noPaginationDataProvider->models, 'technic_id')])->asArray()->all(), 'id', 'name'),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ],
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'rental_period_begin',
                'format' => ['date', 'php:d.m.Y'],
                'label' => 'Аренда С',
                'width' => '6%',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'rental_period_end',
                'format' => ['date', 'php:d.m.Y'],
                'label' => 'Аренда По',
                'width' => '6%',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'objects_id',
                'content' => function($data){
                    $contract = ArrayHelper::getColumn(Objects::find()->where(['id' => $data->objects_id])->all(), 'name');
                    return implode('',$contract);
                },
                'filter' => \app\models\Bid::getObjectsList(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ],
            ],
//    [
//        'attribute' => 'status_id',
//        'value' => function ($model, $key, $index, $widget) {
//            $statusName = implode('',ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'name'));
//            $statusColor = implode('', ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'color'));
//            return "<div style='background-color: {$statusColor};color: black; text-align: center;'> <span>{$statusName}</span></div>";
//
//        },
//        'width' => '6%',
//        'filter' => ArrayHelper::map(BidStatus::find()->asArray()->all(), 'id', 'name'),
//        'filterType' => 'dropdown',
//        'format' => 'raw',
//        'hAlign' => GridView::ALIGN_CENTER,
//    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'shift',
//                        'label' => 'Смены',
//                        'width' => '4%',
//                    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'hours',
//        'label' => 'Часов',
//        'content' => function($data){
//            $hours = $data->hours;
//            if ($hours == 1){
//                return 'Сутки';
//            }else{
//                return $hours;
//            }
//        },
//        'width' => '4%',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'delivery_from',
//    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'sn_num',
//                    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'contract_num',
//                        'visible' => Yii::$app->user->identity->isSuperAdmin()
//                    ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'created_at',
                'label' => 'Создана',
                'format' => ['date', 'php:H:i d.m.Y'],
                'width' => '6%',
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign'=>'middle',
                'template' => '{update} {delete} {clone}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to(['bid/'.$action,'id'=>$key]);
                },
                'buttons' => [
                    'clone' => function ($url,$model,$key) {
                        return Html::a( '<i class="fa fa-files-o" style="font-size: 16px;"title="Клонировать"data-toggle="tooltip"></i>', ['bid/clone', 'id' => $model->id], ['role' => 'modal-remote']);
                    },
                    'update' => function ($url, $model, $key) {
//                                if((Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER && ArrayHelper::getValue($model, 'status.name') != "В работе") && Yii::$app->user->identity->company->type != \app\models\Company::TYPE_PROVIDER){
//                                if((Yii::$app->user->identity->isSuperAdmin() || ArrayHelper::getValue($model, 'status.name') != "В работе")){
                        return Html::a( '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['bid/update', 'id' => $model->id], ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
//                                }
                    },
                ],
                'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
                'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
                'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Удаление',
                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
            ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Вы уверены?',
//            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
//    ],

        ];
    } elseif(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER) {
        $columns = [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'width' => '20px',
            ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'id',
                'width' => '3%',
                'hAlign' => GridView::ALIGN_CENTER,
            ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'technic_id',
                'content' => function($data){
                    $contract = ArrayHelper::getColumn(Technic::find()->where(['id' => $data->technic_id])->all(), 'name');
                    return implode('',$contract);

                },
                'filter' => ArrayHelper::map(Technic::find()->where(['id' => ArrayHelper::getColumn($noPaginationDataProvider->models, 'technic_id')])->asArray()->all(), 'id', 'name'),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ],
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'rental_period_begin',
                'format' => ['date', 'php:d.m.Y'],
                'label' => 'Аренда С',
                'width' => '6%',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'rental_period_end',
                'format' => ['date', 'php:d.m.Y'],
                'label' => 'Аренда По',
                'width' => '6%',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'objects_id',
                'content' => function($data){
                    $contract = ArrayHelper::getColumn(Objects::find()->where(['id' => $data->objects_id])->all(), 'name');
                    return implode('',$contract);
                },
                'filter' => \app\models\Bid::getObjectsList(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ],
            ],
//    [
//        'attribute' => 'status_id',
//        'value' => function ($model, $key, $index, $widget) {
//            $statusName = implode('',ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'name'));
//            $statusColor = implode('', ArrayHelper::getColumn(BidStatus::find()->where(['id' => $model->status_id])->all(), 'color'));
//            return "<div style='background-color: {$statusColor};color: black; text-align: center;'> <span>{$statusName}</span></div>";
//
//        },
//        'width' => '6%',
//        'filter' => ArrayHelper::map(BidStatus::find()->asArray()->all(), 'id', 'name'),
//        'filterType' => 'dropdown',
//        'format' => 'raw',
//        'hAlign' => GridView::ALIGN_CENTER,
//    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'shift',
//                        'label' => 'Смены',
//                        'width' => '4%',
//                    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'hours',
//        'label' => 'Часов',
//        'content' => function($data){
//            $hours = $data->hours;
//            if ($hours == 1){
//                return 'Сутки';
//            }else{
//                return $hours;
//            }
//        },
//        'width' => '4%',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'delivery_from',
//    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'sn_num',
//                    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'contract_num',
//                        'visible' => Yii::$app->user->identity->isSuperAdmin()
//                    ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'customer',
                'content' => function($data){
//            $company = ArrayHelper::getColumn(Company::find()->where(['id' => $data->created_by])->all(), 'name');
//            return implode('',$company);
                    $companies = ArrayHelper::map(Company::find()->where(['type' => 0])->all(), 'id', 'name');

                    if(isset($companies[$data->customer])){
                        return $companies[$data->customer];
                    }

                    return null;
                },
                'filter' => ArrayHelper::map(Company::find()->where(['id' => ArrayHelper::getColumn($noPaginationDataProvider->models, 'customer')])->all(), 'id', 'name'),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ],
//                'visible' => Yii::$app->user->identity->isSuperAdmin()
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'created_at',
                'label' => 'Создана',
                'format' => ['date', 'php:H:i d.m.Y'],
                'width' => '6%',
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign'=>'middle',
                'template' => '{update} {delete} {clone}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to(['bid/'.$action,'id'=>$key]);
                },
                'buttons' => [
                    'clone' => function ($url,$model,$key) {
                        return Html::a( '<i class="fa fa-files-o" style="font-size: 16px;"title="Клонировать"data-toggle="tooltip"></i>', ['bid/clone', 'id' => $model->id], ['role' => 'modal-remote']);
                    },
                    'update' => function ($url, $model, $key) {
//                                if((Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER && ArrayHelper::getValue($model, 'status.name') != "В работе") && Yii::$app->user->identity->company->type != \app\models\Company::TYPE_PROVIDER){
//                                if((Yii::$app->user->identity->isSuperAdmin() || ArrayHelper::getValue($model, 'status.name') != "В работе")){
                        return Html::a( '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['bid/update', 'id' => $model->id], ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
//                                }
                    },
                ],
                'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
                'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
                'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Удаление',
                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
            ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Вы уверены?',
//            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
//    ],

        ];
    }
}



?>

    <style>
        <?php foreach (BidStatus::find()->all() as $bidStatus): ?>
            .status-<?=$bidStatus->id?> td{
                background: <?=$bidStatus->color?> !important;
                border-color: <?=\app\components\helpers\ColorManager::darkenColor($bidStatus->color, 1.2)?> !important;
            }
        <?php endforeach; ?>
    </style>

    <div class="panel panel-inverse">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?php foreach (BidStatus::find()->all() as $bidStatus): ?>
                        <?= Html::a($bidStatus->name, ['bid/index', 'BidSearch[status_id]' => $bidStatus->id], ['class' => $searchModel->status_id == $bidStatus->id ? 'btn btn-success' : 'btn btn-default']) ?>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </div>


<div class="panel panel-inverse position-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Заявки</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php if(Yii::$app->user->identity->isSuperAdmin() or Yii::$app->user->identity->company->type == 0): ?>
                <?= Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    [
                        'role' => 'modal-remote',
                        'title' => 'Добавить заявку',
                        'class' => 'btn btn-success'
                    ]) . '&nbsp;'
                ?>
            <?php endif; ?>
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'hover' => true,
                'rowOptions' => function($model){
                    return ['style' => 'cursor: pointer;', 'class' => "status-{$model->status_id}"];
                },
                'columns' => $columns,

                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                ]).' '.Html::a('<i class="glyphicon glyphicon-lock"></i>&nbsp; Закрыть',
                                    ["bulk-close"] ,
                                    [
                                        "class"=>"btn btn-warning btn-xs",
                                        'role'=>'modal-remote-bulk',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите закрыть данный элемент?'
                                    ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>

        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg','tabindex' => false],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>



<?php

$script = <<< JS
$('[data-key]').click(function(e){
    if($(e.target).is('td')){
        var id = $(this).data('key');
        window.location = '/bid/view?id='+id;
    }
});

$(document).on('pjax:complete', function(event) {
    $('[data-key]').click(function(e){
        if($(e.target).is('td')){
            var id = $(this).data('key');
            window.location = '/bid/view?id='+id;
        }
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>