<?php

use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Technic */
?>
<div class="technic-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'model',
            'characteristics',
            'equipment',
            [
                'attribute' => 'type_id',
                'value' => ArrayHelper::getValue($model->getType()->one(),'name')
            ],
            [
                'attribute' => 'subgroup_id',
                'value' => ArrayHelper::getValue($model->getSubgroup()->one(),'name')
            ],
        ],
    ]) ?>

</div>
