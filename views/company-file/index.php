<?php

use kartik\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanyFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<?php \yii\widgets\Pjax::begin(['id' => 'files-pjax-container', 'enablePushState' => false]) ?>

<?= GridView::widget([
    'id' => 'files-crud-datatable',
    'dataProvider' => $dataProvider,
    //            'filterModel' => $searchModel,
    'pjax' => false,
    'responsiveWrap' => false,
    'columns' => require(__DIR__ . '/_columns.php'),
    'panelBeforeTemplate' => null,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'responsiveWrap' => false,
    'panel' => null,
]) ?>

<?php \yii\widgets\Pjax::end() ?>

    <div class="hidden">
        <?php $model = new \app\models\CompanyFile();
        $form = ActiveForm::begin(['id' => 'company-file-form']) ?>
        <?= $form->field($model, 'company_id')->textInput() ?>
        <?= $form->field($model, 'type')->textInput() ?>
        <?= $form->field($model, 'file')->fileInput() ?>
        <?php ActiveForm::end() ?>
    </div>

<?php

$script = <<< JS
$("#company-file-form input[type='file']").change(function(e){
    
    var data = new FormData($('#company-file-form')[0]);  
 
    $.ajax({
        url: '/company/upload-file',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST', // For jQuery < 1.9
        success: function(data){
            if(data.length == 0){
                $.pjax.reload('#files-pjax-container');
            } else {
                alert('Возникла ошибка при загрузке фото');
            }
        }
    });
});
JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>