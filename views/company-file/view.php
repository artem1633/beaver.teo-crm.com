<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyFile */
?>
<div class="company-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'type',
            'name',
            'path',
            'created_at',
        ],
    ]) ?>

</div>
