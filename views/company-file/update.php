<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyFile */
?>
<div class="company-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
