<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){
            if($model->type != null){
                $name = ArrayHelper::getValue(\app\models\CompanyFile::typeLabels(), $model->type);
            } else {
                $name = $model->name;
            }

            if($model->path == null){
                return $name;
            } else {
                return Html::a($name, '/'.$model->path, ['target' => '_blank', 'data-pjax' => '0']);
            }
        },
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'path',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{upload}',
        'buttons' => [
            'upload' => function($url, $model) {
                return Html::a('<i class="fa fa-upload text-success"></i>', '#', ['title' => 'Загрузить', 'onclick' => '
                    var companyId = '.$model->company_id.';
                    var type = '.$model->type.';
                    
                    $("#companyfile-company_id").val(companyId);
                    $("#companyfile-type").val(type);
                    
                    $("#company-file-form input[type=\'file\']").trigger("click");
                ']);
            },
//            'delete' => function ($url, $model) {
//                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Удалить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                    'data-confirm-title'=>'Вы уверены?',
//                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
//                ]);
//            },
//            'update' => function ($url, $model) {
//                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            }
        ],
    ],

];   