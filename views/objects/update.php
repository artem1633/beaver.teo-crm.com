<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Objects */
?>
<div class="objects-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
