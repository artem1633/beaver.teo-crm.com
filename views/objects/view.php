<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Objects */
?>
<div class="objects-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address',
            'contact_name',
            'contact_phone',
            'access_time',
        ],
    ]) ?>

</div>
