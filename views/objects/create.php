<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Objects */

?>
<div class="objects-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
