<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Timesheet */

?>
<div class="timesheet-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
