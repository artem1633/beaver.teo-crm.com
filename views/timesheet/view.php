<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Timesheet */
?>
<div class="timesheet-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'hours',
            'bid_id',
        ],
    ]) ?>

</div>
