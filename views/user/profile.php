<?php

use app\models\Company;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */


$this->title = 'Профиль';

?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Настройки</h4>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin() ?>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'last_name')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'name')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'patronymic')->textInput() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'phone')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'email')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'position_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Position::find()->where(['company_id' => $model->company_id])->all(), 'id', 'name')) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= \yii\helpers\Html::submitButton('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <img class="circle-img" style="height: 250px; width: 250px; border-radius: 100%; object-fit: contain; border: 2px solid #cecece; cursor: pointer;" src="/<?=$model->getRealAvatarPath()?>" data-role="profile-image-select">
                    </div>
                    <div class="col-md-6">
                        <p>Название компании</p>
                        <h4><?=ArrayHelper::getValue(Company::find()->where(['id' => $model->company_id])->one(),'name');?></h4>
                        <p style="margin-top: 30px">Тип компании компании</p>
                        <h4><?php
                            $type = ArrayHelper::getValue(Company::find()->where(['id' => $model->company_id])->one(),'type');
                                        if ($type === null){
                                        echo "Супер Компания";
                                        }elseif ($type === 0){
                                        echo "Компания покупатель";
                                        }else{
                                        echo  "Компания поставщик";
                                        }
                            ?></h4>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php

$script = <<< JS
    $('[data-role="profile-image-select"]').click(function(){
        $('#avatar-form input').trigger('click');
    });

    $('#avatar-form input').change(function(){
        $('#avatar-form').submit();
    });
    
    $('#avatar-form').submit(function(e){
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            url: $('#avatar-form').attr('action'),
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                if(response.success === 1){
                    var path = '/'+response.path;
                    $('[data-role="avatar-view"]').each(function(i){
                        $(this).attr('src', path);
                    });
                    $('[data-role="profile-image-select"]').each(function(i){
                        $(this).attr('src', path);
                    });
                }
            }
        });
        e.preventDefault();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>