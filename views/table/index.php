<?php

use app\admintheme\grid\GridView;
use app\models\BidStatus;
use app\models\Company;
use app\models\Objects;
use app\models\Technic;
use app\models\TechnicType;
use app\models\TechnicTypeSubgroup;
use kartik\select2\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Табель';

\app\assets\plugins\TipperAsset::register($this);


$companiesExecutors = Company::find()->where(['type' => 1])->all(); // 1 - Исполнитель
$companiesCustomers = Company::find()->where(['type' => 0])->all(); // 1 - Заказчик

if(Yii::$app->user->identity->company->type != Company::TYPE_PROVIDER){
    $customerPks = ArrayHelper::getColumn(\app\models\Bid::find()->where(['executor_id' => Yii::$app->user->getId()])->all(), 'customer');

    $companiesCustomers = Company::find()->where(['type' => 0, 'id' => $customerPks])->all();
}

if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER && Yii::$app->user->identity->isSuperAdmin() == false){
    $executorPks = array_unique(ArrayHelper::getColumn(\app\models\Bid::find()->where(['customer' => Yii::$app->user->identity->company->id])->all(), 'executor_id'));
    $companiesExecutors = Company::find()->where(['id' => $executorPks])->all();
}


?>
    <div class="table-tabs"style="margin-top: -45px">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Фильтры</h4>
                        <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php $form = ActiveForm::begin(['method' => 'GET']); ?>

                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($searchModel, 'customer')->widget(Select2::class, [
                                        'data' => ArrayHelper::map($companiesCustomers, 'id', 'name'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'placeholder' => 'Выберите',
                                        ],
                                    ]) ?>
                                </div>
                                <?php if(Yii::$app->user->identity->company->type != Company::TYPE_PROVIDER): ?>
                                    <div class="col-md-6">
                                        <?= $form->field($searchModel, 'executor_id')->widget(Select2::class, [
                                            'data' => ArrayHelper::map($companiesExecutors, 'id', 'name'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => 'Выберите',
                                            ],
                                        ]) ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($searchModel, 'objects_id')->widget(Select2::class, [
                                'data' => ArrayHelper::map(Objects::find()->all(), 'id', 'name'),
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => 'Выберите',
                                ],
                            ]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($searchModel, 'rental_period_begin')->input('date') ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($searchModel, 'rental_period_end')->input('date') ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($searchModel, 'status_id')->widget(Select2::class, [
                                'data' => ArrayHelper::map(BidStatus::find()->all(), 'id', 'name'),
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => 'Выберите',
                                ],
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($searchModel, 'type_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(TechnicType::find()->all(), 'id', 'name'),
                                'language' => 'ru',
                                'options' => ['placeholder' => 'выберите тип ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],'pluginEvents' => [
                                    "change:select" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-kind?q='+$(this).val(),
                                success: function(response){

                                    var data = '';

                                     $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                }
                            });
                        }",
                                    "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-kind?q='+$(this).val(),
                                success: function(response){

                                    var data = '';
                                    
                                    var currentValue = $('#bidsearch-type_id').val();

                                    $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                    $('#bidsearch-subgroup_id').html(data);
                                    
                                    $('#bidsearch-subgroup_id').val(currentValue);
                                }
                            });   
                        }",
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($searchModel, 'subgroup_id')->widget(Select2::classname(), [
                                'data' =>  ArrayHelper::map(TechnicTypeSubgroup::find()->all(), 'id', 'name'),
                                'language' => 'ru',
                                'options' => ['placeholder' => 'выберите подгруппу ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],'pluginEvents' => [
                                    "change:select" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-tech?q='+$(this).val(),
                                success: function(response){

                                    var data = '';

                                     $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                }
                            });
                        }",
                                    "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/bid/search-tech?q='+$(this).val(),
                                success: function(response){

                                    var data = '';
                                    
                                    var currentValue = $('#bidsearch-subgroup_id').val();

                                    $.each(response,function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
                                    
                                    console.log(data);
                                    
                                    $('#bidsearch-technic_id').html(data);
                                    
                                    $('#bidsearch-technic_id').val(currentValue);
                                }
                            });   
                        }",
                                ],
                            ]);
                            ?>

                        </div>
                        <div class="col-md-4">
                            <?= $form->field($searchModel, 'technic_id')->widget(Select2::classname(), [
                                'data' =>  ArrayHelper::map(Technic::find()->all(), 'id', 'name'),
                                'language' => 'ru',
                                'options' => ['placeholder' => 'выберите технику ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>

                        <div class="col-md-12">
                            <?= Html::a('Сбросить', ['table/index'], ['class' => 'btn btn-default', 'onclick' => '$.get("/table/clear-session", function(){});']) ?>
                            <?= Html::submitButton('Применить', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end() ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Табель</h4>
                        <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" >
                        <div class="col-md-12">
                            <?php $this->beginContent('@app/views/table/status.php',[
                                //                            'model' => $model,
                                'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                            ]); ?>
                            <?php $this->endContent(); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



<?php

$script = <<< JS
// data-popup-role

      tippy('[data-popup-role]', {
        content: 'Загрузка ...',
        allowHTML: true,
        onShow: function(){
            
            
            
            
        }
      });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>