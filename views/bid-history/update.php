<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BidHistory */
?>
<div class="bid-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
