<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BidHistory */
?>
<div class="bid-history-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'bid_id',
            'history_comment:ntext',
            'created_at',
        ],
    ]) ?>

</div>
