<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'num')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'invoice_date')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>
        </div>


        <div class="col-md-3">
            <?= $form->field($model, 'pay_date')->input('date') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 hidden">
            <?= $form->field($model, 'date_from')->input('date') ?>
        </div>
        <div class="col-md-3 hidden">
            <?= $form->field($model, 'date_to')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'upd_num')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'upd_date')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sum_not_payed')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'delivery')->checkbox() ?>
        </div>
    </div>
    <div class="row hidden">
        <div class="col-md-12">
            <?= $form->field($model, 'status')->dropDownList([
                'запрошено'  => 'запрошено',
                'выставленно'  => 'выставленно',
                'частично оплачено' => 'частчно оплачено',
                'оплачено' => 'оплачено',
                'оплачено и закрыто' => 'оплачено и закрыто',
            ]) ?>
        </div>
    </div>








  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
