<?php

use app\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'num',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'invoice_date',
        'format' => ['date', 'php:d.m.Y'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){
            $color = '';
            if($model->status == 'выставленно'){
                $color = '#d22626';
            } else if($model->status == 'частично оплачено'){
                $color = '#d29e26';
            } else if($model->status == 'оплачено'){
                $color = '#cfcc30';
            } else if($model->status == 'оплачено и закрыто'){
                $color = '#16c925';
            } else if($model->status == 'оплачено и закрыто'){
                $color = '#16c925';
            } else if($model->status == 'запрошено'){
                $color = '#787878';
            }

//            if ($model->status === 'не оплачено'){
//               return "<span class='badge'  style='background-color: red'>$model->status </span>";
//            }elseif ($model->status === 'оплачено'){
//                return "<span class='badge'  style='background-color: green'>$model->status </span>";
//            }elseif ($model->status === 'частично оплачен'){
//                return "<span class='badge'  style='background-color: orange'>$model->status </span>";
//            }elseif ($model->status === 'закрыт'){
//                return "<span class='badge'  style='background-color: lightseagreen'>$model->status </span>";
//            }

            return "<span class='badge' style='background-color: {$color};'>$model->status</span>";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pay_date',
        'format' => ['date', 'php:d.m.Y'],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'date_from',
//        'format' => ['date', 'php:d.m.Y'],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'date_to',
//        'format' => ['date', 'php:d.m.Y'],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'upd_num',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'upd_date',
//        'format' => ['date', 'php:d.m.Y'],
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function($model){
            if($model->company){
                $output = $model->company->name;
                $output .= $model->company->type == Company::TYPE_BUYER ? "<p><span class='label label-success'>Покупатель</span></p>" : "<p><span class='label label-warning'>Поставщик</span></p>";
                return $output;
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Действия',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function($model){
            return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['invoice/delete', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'
                ]) . ' ' . Html::a('<i class="glyphicon glyphicon-pencil" style="font-size: 16px;"></i>', ['invoice/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                    'role'=>'modal-remote','title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                ]) . ' ' . Html::a('<i class="glyphicon glyphicon-copy" style="font-size: 16px;"></i>', ['invoice/copy', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                    'role'=>'modal-remote','title'=>'Скопировать',
                    'data-confirm'=>false, 'data-method'=>false,
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                ]);
        }
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
////        'viewOptions'=>['title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Вы уверены?',
//            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
//    ],


];   