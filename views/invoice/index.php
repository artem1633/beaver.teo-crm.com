<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BidHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $candidateId integer */

if(isset($ajaxModal) == false){
    $ajaxModal = true;
}

CrudAsset::register($this);

?>
<?=GridView::widget([
    'id'=>'history-datatable',
    'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
    'pjax'=>true,
    'columns' => require(__DIR__.'/_columns.php'),

    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'responsiveWrap' => false,
//    'panel' => [
//        'headingOptions' => ['style' => 'display: none;'],
//        'after'=>'',
//    ]
])?>
<?php if($ajaxModal): ?>
    <?php Modal::begin([
        "id"=>"ajaxCrudModal",
        'options' => ['class' => 'fade modal-slg','tabindex' => false],
        "footer"=>"",// always need it for jquery plugin
    ])?>
    <?php Modal::end(); ?>
<?php endif; ?>
