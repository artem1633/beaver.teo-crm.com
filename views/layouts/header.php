<?php

use app\models\Bid;
use app\models\Characteristic;
use app\models\Objects;
use app\models\Technic;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                Beaver
            </a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- end mobile sidebar expand / collapse button -->
        <?php if(Yii::$app->user->isGuest == false): ?>
            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
                <li class="date-now"><?php echo date('d.m.Y H:i'); ?></li>
                <li class="navbar-user" style="padding-top: 20px"><?=Yii::$app->user->identity->company->name?></li>

                <?php
                $beforeDate = new DateTime();
                $beforeDate->modify('+3 day');
                $endDate = date_format($beforeDate, 'Y-m-d');
                $beforeDate = date('Y-m-d');

                $bids = Bid::find();

                $beforeDateBids = Bid::find()->andWhere(['between', 'rental_period_begin', $beforeDate, $endDate])->joinWith(['author'])->all();
                $endDateBids = Bid::find()->andWhere(['between', 'rental_period_end', $beforeDate, $endDate])->joinWith(['author'])->all();

                if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 0){
                    $bids = Bid::find();
                    $bids->andWhere(['created_by' => Yii::$app->user->identity->company_id]);

                    $beforeDateBids = $bids->andWhere(['between', 'rental_period_begin', $beforeDate, $endDate])->joinWith(['author'])->all();

                    $bids = Bid::find();
                    $bids->andWhere(['created_by' => Yii::$app->user->identity->company_id]);

                    $endDateBids = $bids->andWhere(['between', 'rental_period_end', $beforeDate, $endDate])->joinWith(['author'])->all();
                }
                if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 1){
                    $technicLike = ArrayHelper::getColumn(Characteristic::find()
                        ->where(['company_id' => Yii::$app->user->identity->company_id])->all(),'technic_id');

                    $bids = Bid::find();
                    $bids->orFilterWhere(['and',['in','technic_id',$technicLike],['is','executor_id',null]]);
                    $bids->orFilterWhere(['executor_id' => Yii::$app->user->identity->company->id]);

                    $beforeDateBids = $bids->andWhere(['between', 'rental_period_begin', $beforeDate, $endDate])->joinWith(['author'])->all();
//                    \Yii::warning(Yii::$app->user->identity->company->id, 'raw sql');
//                    $beforeDateBids = $beforeDateBids->all();

                    $bids = Bid::find();
                    $bids->orFilterWhere(['and',['in','technic_id',$technicLike],['is','executor_id',null]]);
                    $bids->orFilterWhere(['executor_id' => Yii::$app->user->identity->company->id]);

                    $endDateBids = $bids->andWhere(['between', 'rental_period_end', $beforeDate, $endDate])->joinWith(['author'])->all();
                }




                $notificationCount = count($beforeDateBids) + count($endDateBids);
                $notificationSpan = $notificationCount == 0 ? '' : '<span style="position: absolute; display: inline-block; color: #fff; background: #ff5b57; padding: 0px 5px; font-size: 11px; border-radius: 144%; left: 28px; top: 24px;">'.$notificationCount.'</span>';

                $invoiceData = [];
                $newBids = [];

                if(Yii::$app->user->identity->company->type == \app\models\Company::TYPE_BUYER){
                    $myBidPks = ArrayHelper::getColumn(Bid::find()->where(['or', ['customer' => Yii::$app->user->getId()], ['created_by' => Yii::$app->user->identity->company_id]])->all(), 'id');
                    $invoiceData = \app\models\Invoice::find()->where(['bid_id' => $myBidPks])->andWhere(['invoice_date' => date('Y-m-d')])->all();

                    $myTechnic = ArrayHelper::getColumn(Characteristic::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'technic_id');

                    $newBids = Bid::find()->where(['between', 'created_at', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])->andWhere(['technic_id' => $myTechnic])->all();
                }


                ?>

                <li class="navbar-user">
                    <?= Html::a('<i class="fa fa-bell" style="font-size: 20px;"></i>'.$notificationSpan, '#', ['title' => 'Уведомления', 'style' => 'cursor: pointer; position: relative;', 'onclick' => "event.preventDefault(); $('.header-messages').toggle();"]) ?>
                </li>

                <li class="dropdown navbar-user">
                    <div class="header-messages" style="display: none;position: absolute;background: #fff;border-radius: 3px;box-shadow: 0 0 10px rgba(0,0,0,0.5);height: 260px;width: 260px;margin-top: 44px;right: 1px;overflow-y: scroll;">
                        <?php foreach($newBids as $bid): ?>
                            <a href="/bid/view?id=<?=$bid->id?>" class="header-message" style="display: inline-block; padding: 4px 12px; border-bottom: 1px solid #969696;">
                                <div class="header-message-title" style="color: #474747;font-weight: 600;">Новый заказ</div>
                                <p style="color: #474747;">Вам выставлен новый заказ №<?=$bid->id?></p>
                            </a>
                        <?php endforeach; ?>
                        <?php foreach($invoiceData as $invoiceDatum): ?>
                            <a href="/bid/view?id=<?=$invoiceDatum->bid_id?>" class="header-message" style="display: inline-block; padding: 4px 12px; border-bottom: 1px solid #969696;">
                                <div class="header-message-title" style="color: #474747;font-weight: 600;">Новый счет</div>
                                <p style="color: #474747;">Вам выставлен новый счет №<?=$invoiceDatum->id?></p>
                            </a>
                        <?php endforeach; ?>
                        <?php foreach ($beforeDateBids as $beforeDateBid):?>
                            <?php
                            $obj = ArrayHelper::getValue(Objects::find()->where(['id' => $beforeDateBid->objects_id])->one(),'name');
                            $tech = ArrayHelper::getValue(Technic::find()->where(['id' => $beforeDateBid->technic_id])->one(),'name');
                            $titleText = 'Подходит срок начала аренды';
                            $notificationText = "У заявки {$beforeDateBid->id}
                                подходит срок начала аренды {$beforeDateBid->rental_period_begin}.
                                Объект - {$obj},
                                Техника - {$tech}";
                            ?>
                            <a href="/bid/view?id=<?=$beforeDateBid->id?>" class="header-message" style="display: inline-block; padding: 4px 12px; border-bottom: 1px solid #969696;">
                                <div class="header-message-title" style="color: #474747;font-weight: 600;"><?=$titleText?></div>
                                <p style="color: #474747;"><?=$notificationText?></p>
                            </a>
                        <?php endforeach; ?>
                        <?php foreach ($endDateBids as $endDateBid):?>
                            <?php
                            $obj = ArrayHelper::getValue(Objects::find()->where(['id' => $endDateBid->objects_id])->one(),'name');
                            $tech = ArrayHelper::getValue(Technic::find()->where(['id' => $endDateBid->technic_id])->one(),'name');
                            $titleText = 'Подходит срок окончания аренды';
                            $notificationText = "У заявки <a href=\"/bid/view?id=$endDateBid->id\">{$endDateBid->id} </a> 
                                    подходит срок окончания аренды {$endDateBid->rental_period_end}.
                                    Объект - {$obj},
                                    Техника - {$tech}
                                    ";
                            ?>
                            <div class="header-message"style=" padding: 4px 12px;border-bottom: 1px solid #969696;">
                                <div class="header-message-title" style="color: #474747;font-weight: 600;"><?=$titleText?></div>
                                <p style="color: #474747;"><?=$notificationText?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </li>



                <li class="dropdown navbar-user">
                    <a id="btn-user-dropdown" href="javascript:;">
                        <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->email?></span> <b class="caret"></b>
                    </a>
                    <ul id="dropdown-user-menu" class="dropdown-menu animated fadeInLeft" style="">
                        <li class="arrow"></li>
                        <li> <?= Html::a('Настройки пользователи', ['user/profile']) ?> </li>
                        <li> <?= Html::a('Тарифы', ['#']) ?> </li>
                        <li class="divider"></li>
                        <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                    </ul>

                </li>
            </ul>
            <!-- end header navigation right -->
        <?php endif; ?>
    </div>
    <!-- end container-fluid -->
</div>

<!-- Cleversite chat button -->
<script type='text/javascript'>
    (function() {
        var s = document['createElement']('script');
        s.type = 'text/javascript';
        s.async = true;
        s.charset = 'utf-8';
        s.src = '//cleversite.ru/cleversite/widget_new.php?supercode=1&referer_main='+encodeURIComponent(document.referrer)+'&clid=58510bRHNK&siteNew=76381';
        var ss = document['getElementsByTagName']('script')[0];
        if(ss)
        {
            ss.parentNode.insertBefore(s, ss);
        } else {
            document.documentElement.firstChild.appendChild(s);
        };
    })();
</script>
<!-- / End of Cleversite chat button -->