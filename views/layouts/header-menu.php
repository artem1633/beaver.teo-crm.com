<?php

use app\admintheme\widgets\Menu;

?>

<div id="sidebar" class="sidebar">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo Menu::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav'],
                    'items' => [
                        //                    ['label' => 'Пользователи', 'icon' => 'fa fa-user-o', 'url' => ['/user'],],
                        // ['label' => 'Кабинет администратора', 'url' => ['/admin-files'], 'icon' => 'fa fa-file', 'visible' => Yii::$app->user->identity->isSuperAdmin()],

                        ['label' => 'Заявки', 'url' => ['/bid'], 'icon' => 'fa fa-briefcase'],
                        [
                            'label' => 'Моя компания',
                            'url' => ['/company'],
                            'icon' => 'fa fa-building-o',
                            'visible' => Yii::$app->user->identity->isSuperAdmin() == false
                        ],
                        [
                            'label' => 'Моя техника',
                            'icon' => 'fa fa-bus',
                            'url' => ['/characteristic'],
                            'visible' => Yii::$app->user->identity->company->type == 1 or Yii::$app->user->identity->isSuperAdmin()
                        ],
                        ['label' => 'Пользователи', 'url' => ['/user'], 'icon' => 'fa fa-users'],
                        [
                            'label' => 'Компании',
                            'icon' => 'fa fa-building-o',
                            'url' => ['/company'],

                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],

                        ['label' => 'Обратная связь', 'url' => ['/ticket'], 'icon' => 'fa fa-question'],
                        ['label' => 'Табель', 'url' => ['/table'], 'icon' => '  fa fa-table'],

                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                ['label' => 'Должности', 'url' => ['/position']],
                                [
                                        'label' => 'Техника',
                                        'url' => ['/technic'],
                                        'visible' => Yii::$app->user->identity->isSuperAdmin()
                                ],
                                [
                                        'label' => 'Объекты',
                                        'url' => ['/objects'],
                                        'visible' => Yii::$app->user->identity->company->type == 0 or Yii::$app->user->identity->isSuperAdmin()
                                ],
                                [
                                        'label' => 'Тип техники',
                                        'url' => ['/technic-type'],
                                        'visible' => Yii::$app->user->identity->isSuperAdmin()
                                ],
//                                [
//                                        'label' => 'Статус заявки',
//                                        'url' => ['/bid-status'],
//                                        'visible' =>  Yii::$app->user->identity->company->type == 1 or Yii::$app->user->identity->isSuperAdmin()
//                                ],
                                [
                                    'label' => 'Документация АПИ',
                                    'url' => ['/api/doc'],
                                    'visible' => Yii::$app->user->identity->company->api_key ?? false
                                ],
                            ],
                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
