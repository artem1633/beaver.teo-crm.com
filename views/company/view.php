<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Company */


CrudAsset::register($this);

?>
<div class="company-view">

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Основная информация</h4>
                </div>
                <div class="panel-body">
                    <?php Pjax::begin(['id' => 'company-view-pjax']) ?>
                    <div class="col-md-6">
                        <h4>Реквизиты</h4>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'name',
                                'email',
                                'address',
                                'official_address',
                                'contract_num',
                                'director',
                                'inn',
                                'ogrn',
                                'kpp',
                                'phone',
                                'site',
                                [
                                        'attribute' => 'type',
                                        'value' => function($model){
                                            if($model->type === 1){
                                                return 'Поставщик';
                                            }elseif($model->type === 0){
                                                return 'Покупатель';
                                            }else{
                                                return 'Суперкомпания';
                                            }
                                        }
                                ]
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <h4>Банковские реквизиты</h4>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'bank_name',
                                'bank_bik',
                                'bank_address',
                                'bank_correspondent_account',
                                'bank_register_number',
                                [
                                    'attribute' => 'bank_registration_date',
                                    'format' => ['date', 'php:d.m.Y'],
                                ],
                                'bank_payment_account',
                            ],
                        ]) ?>
                    </div>
                    <?php if (Yii::$app->user->identity->isCompanyAdmin() && $model->api_key): ?>
                        <div class="col-md-6 api-info">
                            <p>АПИ ключ: </p>
                            <p><b id="api-key"><?= $model->api_key ?></b></p>
                            <?= Html::button('Сгенерировать заново', [
                                'id' => 'generate-key',
                                'class' => 'btn btn-warning btn-xs pull-right'
                            ]) ?>
                        </div>
                    <?php else: ?>
                        <div class="col-md-6 api-info text-warning">
                            <p>Доступ к АПИ запрещен, для получения доступа обратитесь к администратору системы </p>
                        </div>
                    <?php endif; ?>
                    <?php Pjax::end() ?>

                </div>
                <div class="panel-footer">
                    <?= Html::a('Редактировать',
                        ['update', 'id' => $model->id, 'pjaxContainer' => '#company-view-pjax'],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote']) ?>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Сканы документов</h4>
                </div>
                <div class="panel-body">
                    <?= $this->render('@app/views/company-file/index', [
                        'searchModel' => $filesSearchModel,
                        'dataProvider' => $filesDataProvider,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
