<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BidStatus */

?>
<div class="bid-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
