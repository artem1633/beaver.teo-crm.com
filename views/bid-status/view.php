<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BidStatus */
?>
<div class="bid-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>
