<?php


use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\BidStatus;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'attribute' => 'color',
        'value' => function ($model, $key, $index, $widget) {
            return "<span class='badge'  style='background-color: {$model->color}'> </span>";
        },
        'width' => '',
        'filterType' => GridView::FILTER_COLOR,
        'filterWidgetOptions' => [
            'showDefaultPalette' => true,
        ],
        'vAlign' => 'middle',
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'style',
        'filter' => BidStatus::getStyleLabels(),
        'value' => function($model){
            return ArrayHelper::getValue(BidStatus::getStyleLabels(), $model->style);
        },
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Вы уверены?',
//            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
//    ],

];   