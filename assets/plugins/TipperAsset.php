<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class TipperAsset
 * @package app\assets
 */
class TipperAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js',
        'https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
