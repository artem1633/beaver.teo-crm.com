<?php

namespace app\models;

use app\components\MyUploadedFile;
use DatePeriod;
use DateTime;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bid".
 *
 * @property int $id
 * @property string $name Название завки
 * @property int $technic_id Техника
 * @property int $objects_id Объект
 * @property int $status_id Статус
 * @property string $rental_period_begin Срок аренды c
 * @property string $rental_period_end Срок аренды по
 * @property int $shift Количество смен
 * @property int $hours Количество часов
 * @property string $created_at дата создания
 * @property int $created_by автор
 * @property int $executor_id исполнитель
 * @property int $type_id исполнитель
 * @property int $subgroup_id исполнитель
 * @property Objects $objects
 * @property BidStatus $status
 * @property Technic $technic
 * @property $rent_sum Сумма аренды
 * @property $delivery_sum Сумма доставки
 * @property $doc_num Номер закрывающих документов
 * @property $count Количество
 * @property $comment Коментарий
 * @property $pay_status Стаутс оплаты
 * @property $doc_from Период ЗД с
 * @property $doc_to Период ЗД до
 * @property string $delivery_from Срок Доставки c
 * @property string $delivery_to Срок Доставки по
 * @property string $contract_num Номер договора
 * @property integer $sn_num СН погрузчика
 * @property integer $customer заказчик
 * @property string $executor_contract Договор с поставщиком
 * @property int $created_by_user_id Автор (пользователь)
 *
 * @property User $createdByUser
 * @property BidHistory[] $bidHistories
 * @property Invoice[] $invoices
 * @property Timesheet[] $timesheets
 */
class Bid extends \yii\db\ActiveRecord
{
    public $listFile;
    public $file;


    public $dates;

    public $isStatusClosed;

    private $ignoreReWrite = false;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['technic_id', 'objects_id','type_id','subgroup_id','rental_period_begin', 'rental_period_end',], 'required'],
            [['technic_id', 'objects_id','type_id','subgroup_id', 'rental_period_begin', 'rental_period_end'], 'required'],
            ['rental_period_end', function(){
                if($this->rental_period_begin != null && $this->rental_period_end != null){
                    if(strtotime($this->rental_period_begin) > strtotime($this->rental_period_end)){
                        $this->addError('rental_period_end', "Не может быть ранее даты начала");
                    }
                }
            }],
            [['created_by_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by_user_id' => 'id']],
            [['technic_id', 'objects_id', 'status_id', 'shift','count', 'rent_sum','delivery_sum','provider_rent_sum','provider_delivery_sum', 'created_by','executor_id','type_id', 'subgroup_id','sn_num'], 'integer'],
            [['rental_period_begin', 'rental_period_end','contract_num','comment','pay_status', 'created_at', 'delivery_from', 'delivery_to','listFile','customer','contract_num', 'provider_contract_num'], 'safe'],
            [['name', 'executor_contract', 'comment_diller', 'comment_client'], 'string', 'max' => 255],
            [['hours'], 'number'],
            [['objects_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['objects_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BidStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['technic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Technic::className(), 'targetAttribute' => ['technic_id' => 'id']],
            [['dates', 'isStatusClosed'], 'safe']
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
//            [
//                'class' => BlameableBehavior::className(),
//                'updatedByAttribute' => null,
//                'createdByAttribute' => 'created_by',
//                'value' => Yii::$app->user->id
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название завки',
            'technic_id' => 'Техника',
            'objects_id' => 'Объект',
            'status_id' => 'Статус',
            'rental_period_begin' => 'Предполагаемый срок аренды c',
            'rental_period_end' => 'Предполагаемый срок аренды по',
            'shift' => 'Количество смен',
            'hours' => 'Количество часов',
            'created_at' => 'Дата создания',
            'created_by' => 'Автор заявки',
            'executor_id' => 'Исполнитель',
            'type_id' => 'Тип техники',
            'subgroup_id' => 'Подгруппа',
            'delivery_from' =>'Срок доставки с',
            'delivery_to' => 'Срок доставки до',
            'rent_sum' => 'Ставка аренды',
            'delivery_sum' =>'Сумма доставки',
            'contract_num' => 'Номер договора',
            'provider_rent_sum' => 'Ставка аренды',
            'provider_delivery_sum' =>'Сумма доставки',
            'provider_contract_num' => 'Номер договора',
            'count' =>'Количество техники',
            'comment' => 'Коментарий',
            'pay_status' => 'Статус оплаты',
            'isStatusClosed' => 'Закрыта',
            'dates' => 'Срок',
            'executor_contract' => 'Договор с поставщиком',
//            'doc_from' =>'Период ЗД с',
//            'doc_to' => 'Период ЗД до',
            'customer' => 'Заказчик',
            'sn_num' => 'S/N',
            'created_by_user_id' => 'Автор (пользователь)',
            'comment_diller' => 'Комментарий поставщика',
            'comment_client' => 'Комментарий заказчика',
        ];
    }
    public function getDaysCount($ignoreWeekend = false, $provider = false){
        if($ignoreWeekend == false){
            $datetime1 =  new Datetime("$this->rental_period_begin");
            $datetime2 =  new Datetime("$this->rental_period_end");
            $datetime2->modify('+1 day');
            $interval = $datetime1->diff($datetime2)->days;
        } else {

            if($provider){
                $interval = Timesheet::find()->where(['bid_id' => $this->id])->andWhere(['and', ['>', 'provider_hours', 0], ['is not', 'provider_hours', null]])->count();
            } else {
                $interval = Timesheet::find()->where(['bid_id' => $this->id])->andWhere(['and', ['>', 'hours', 0], ['is not', 'hours', null]])->count();
            }
        }



        return $interval;
    }
    public function getNewDate(){
        $date = Timesheet::find()->orderBy(['id' => SORT_DESC])->one()->date;
        $date = new DateTime("$date");
        $date->modify('+1 day');
        (new Timesheet([
            'bid_id' => $this->id,
            'date' => date_format($date, 'Y-m-d'),
        ]))->save(false);
        return date_format($date, 'Y-m-d');
    }

    public function getHoursCount(){
        $bidTime = Timesheet::find()->where(['bid_id' => $this->id])->sum('hours');

        $datetime1 =  new Datetime("$this->rental_period_begin");
        $datetime2 =  new Datetime("$this->rental_period_end");
        $datetime2->modify('+1 day');
        $interval = $datetime1->diff($datetime2)->days;

        return $bidTime;
    }

    public function getRecast($provider = false){
        $normal = $this->getDaysCount(true) * $this->hours;

        if($provider){
            $totalHours = Timesheet::find()->where(['bid_id' => $this->id])->sum('provider_hours');
        } else {
            $totalHours = Timesheet::find()->where(['bid_id' => $this->id])->sum('hours');
        }

        $result = $totalHours - $normal;

        if($result < 0){
            $result = 0;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $daysCount = $this->getDaysCount();
        $this->shift = $daysCount;
        if(isset($_POST['24hours'])){
            if($_POST['24hours'] == 1){
                $this->hours = 1;
            }
        }


        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->company->id;
            $this->created_by_user_id = Yii::$app->user->getId();
        }

        if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER && $this->customer == null){
            $this->customer = Yii::$app->user->identity->company->id;
        }

        if($this->dates != null)
        {
            $dates = explode(' - ', $this->dates);
            $this->rental_period_begin = $dates[0];
            $this->rental_period_end = $dates[1];
        }



        if($this->isNewRecord){
            $status = BidStatus::find()->one();
            $this->status_id = $status->id;
            $users = User::find()->where(['=', 'company_id', 1])->all();
            foreach ($users as $user){
                $subject = "Добавлена новая заявка";
                $htmlBody = "<p>Доброго времени суток!</p><p>Пользователь добавил заявку</p>";
                $user->sendEmailMessage($subject,$htmlBody);
            }
        }


//        var_dump($_POST);
//        exit;


        if (!$this->isNewRecord) {
            $last = Bid::findOne($this->id);
            $this->setChanging($last, $this, false);
        }

        return parent::beforeSave($insert);
    }

    public function getRealId()
    {
//        if($this->isNewRecord){
//            $this->created_by = Yii::$app->user->identity->company->id;
//        }

        return Yii::$app->user->identity->company->id;
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null, $ignoreReWrite = false)
    {
        $this->ignoreReWrite = $ignoreReWrite;
        return parent::save($runValidation, $attributeNames); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);



        if($this->ignoreReWrite == false){
            $oldData = [];
            $oldDataProviders = [];
            if (!$this->isNewRecord) {
                $oldData = ArrayHelper::map(Timesheet::find()->where(['bid_id' => $this->id])->all(), 'date', 'hours');
                $oldDataProviders = ArrayHelper::map(Timesheet::find()->where(['bid_id' => $this->id])->all(), 'date', 'provider_hours');
                Timesheet::deleteAll(['bid_id' => $this->id]);
            }

            $from = new DateTime("$this->rental_period_begin");
            $to = new DateTime("$this->rental_period_end");
            $to->modify('+1 day');
            $period = new DatePeriod(
                $from,
                new \DateInterval('P1D'),
                $to
            );

            \Yii::warning($oldData, '123');

            foreach ($period as $dt) {

                $hours = $this->hours;
                $hoursProvider = null;

                if(isset($oldData[$dt->format('Y-m-d')])){
                    $hours = intval($oldData[$dt->format('Y-m-d')]);
                }

                if(isset($oldDataProviders[$dt->format('Y-m-d')])){
                    $hoursProvider = intval($oldDataProviders[$dt->format('Y-m-d')]);
                }


                (new Timesheet([
                    'bid_id' => $this->id,
                    'date' => $dt->format('y-m-d') . PHP_EOL,
                    'hours' => $hours,
                    'provider_hours' => $hoursProvider,
                ]))->save(false);
            }
            Timesheet::deleteAll(['is', 'date', null]);
        }




        if($insert){
            (new BidHistory([
                'user_id' => Yii::$app->user->identity->id,
                'bid_id' => $this->id,
                'history_comment' => 'Был создан заказ',
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);

            $technic = Technic::findOne($this->technic_id);
            if($technic){
                $characteristics = Characteristic::find()->where(['technic_id' => $technic->id])->all();

                foreach ($characteristics as $characteristic){
                    $providerUser = User::find()->where(['company_id' => $characteristic->company_id, 'is_company_super_admin' => true])->one();
                    if($providerUser){
                        $providerUser->sendEmailMessage(
                            'Новая заявка',
                            'Новая заявка на технику «'.$technic->name.'»'
                        );
                    }
                }
            }
        }

        if($this->listFile != null) {
            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file) {
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'bid_id' => $this->id,
                ]);
                $scan->save(false);
            }
        }
    }
    public function beforeDelete()
    {
        Scan::deleteAll(['bid_id' => $this->id]);
        Invoice::deleteAll(['bid_id' => $this->id]);
        BidHistory::deleteAll(['bid_id' => $this->id]);
        Timesheet::deleteAll(['bid_id' => $this->id]);
        return parent::beforeDelete();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasOne(Objects::className(), ['id' => 'objects_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(BidStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnic()
    {
        return $this->hasOne(Technic::className(), ['id' => 'technic_id']);
    }
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function isWeekend($date) {
        $weekDay = date('w', strtotime($date));
        if ($weekDay == 0 || $weekDay == 6){
            return true;
        }else{
            return false;
        }
    }

    public function getNewRecast(){
        $bidTime = Timesheet::find()->where(['bid_id' => $this->id])->sum('hours');
        $datetime1 =  new Datetime("$this->rental_period_begin");
        $datetime2 =  new Datetime("$this->rental_period_end");
        $datetime2->modify('+1 day');
        $interval = $datetime1->diff($datetime2)->days;
        $hoursCount = $interval * $this->hours;
        $recast = $bidTime - $hoursCount;

        if($recast < 0){
            $recast = 0;
        }

        return $recast;
    }

    public static function getObjectsList()
    {
//        if(Yii::$app->user->identity->isSuperAdmin()){
//            $companyId = null;
//        } else {
//            $companyId = Yii::$app->user->identity->company_id;
//        }
//        $dataCompany = Objects::find()->andFilterWhere(['company_id' => $companyId] )->all();
//        $selectDataCompany = [];
//        foreach($dataCompany as $item){
//            $selectDataCompany[$item->id] = $item->name." ($item->address)";
//        }

//        $selectDataCompany = [];
//
//        if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 0){
//            $bids = Bid::find();
//            $bids->andWhere(['created_by' => Yii::$app->user->identity->company_id]);
//
//            foreach ($bids->all() as $bid)
//            {
//                $selectDataCompany[] = $bid->objects_id;
//            }
//        }
//        if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 1){
//            $technicLike = ArrayHelper::getColumn(Characteristic::find()
//                ->where(['company_id' => Yii::$app->user->identity->company_id])->all(),'technic_id');
//
//            $bids = Bid::find();
//            $bids->orFilterWhere(['and',['in','technic_id',$technicLike],['is','executor_id',null]]);
//            $bids->orFilterWhere(['executor_id' => Yii::$app->user->identity->company->id]);
//
//            foreach ($bids->all() as $bid)
//            {
//                $selectDataCompany[] = $bid->objects_id;
//            }
//        }

        if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 0){
            $searchModel = new BidSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['created_by' => Yii::$app->user->identity->company_id]);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        }elseif(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 1){
            $technicLike = ArrayHelper::getColumn(Characteristic::find()
                ->where(['company_id' => Yii::$app->user->identity->company_id])->all(),'technic_id');

            $searchModel = new BidSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->orFilterWhere(['and',['in','technic_id',$technicLike],['is','executor_id',null]]);
            $dataProvider->query->orFilterWhere(['executor_id' => Yii::$app->user->identity->company->id]);

            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        } else {
            $searchModel = new BidSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        }

        $dataProvider->pagination = false;

        $objects = Objects::find()->where(['id' => ArrayHelper::getColumn($dataProvider->models, 'objects_id')])->all();

        $data = [];

        foreach ($objects as $object){
            $data[$object->id] = $object->name." ($object->address)";
        }


        return $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_user_id']);
    }


    /**
     * @inheritdoc
     * @return BidQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BidQuery(get_called_class());
    }

    public function Change($last, $new)
    {
        $result = "";

        if($last->name != $new->name) $result .= 'Название => '. $new->name."%0A";
        if($last->technic_id != $new->technic_id) $result .= 'Техника => '. $new->technic_id->name."%0A";
        if($last->objects_id != $new->objects_id) $result .= 'Объект => '. $new->objects_id->name."%0A";
        if($last->status_id != $new->status_id) $result .= 'Статус => '. $new->status_id->name."%0A";
        if($last->rental_period_begin != $new->rental_period_begin) $result .= 'Срок аренды с => '. $new->rental_period_begin."%0A";
        if($last->rental_period_end != $new->rental_period_end) $result .= 'Срок аренды по => '. $new->rental_period_end."%0A";
        if($last->shift != $new->shift) $result .= 'Количество смен => '. $new->shift."%0A";
        if($last->hours != $new->hours) $result .= 'Количество часов => '. $new->hours."%0A";
        if($last->executor_id != $new->executor_id) $result .= 'Исполнитель => '. $new->executor_id->name."%0A";
        if($last->type_id != $new->type_id) $result .= 'Тип техники => '. $new->type_id->name."%0A";
        if($last->subgroup_id != $new->subgroup_id) $result .= 'Подгруппа => '. $new->subgroup_id->name."%0A";
        if($last->listfile != $new->listfaie) $result .= 'Загрузка документа => '. $new->file->name."%0A";
        if($last->delivery_from != $new->delivery_from) $result .= 'Срок доставки с => '. $new->delivery_from."%0A";
        if($last->delivery_to != $new->delivery_to) $result .= 'Срок доставки по => '. $new->delivery_to."%0A";
        if($last->rent_sum != $new->rent_sum) $result .= 'Сумма аренды => '. $new->rent_sum."%0A";
        if($last->delivery_sum != $new->delivery_sum) $result .= 'Сумма доставки => '. $new->delivery_sum."%0A";
        if($last->contract_num != $new->contract_num) $result .= 'Номер договора => '. $new->contract_num."%0A";
        if($last->count != $new->count) $result .= 'Количество => '. $new->count."%0A";
        if($last->comment != $new->comment) $result .= 'Комментарий => '. $new->comment."%0A";
        if($last->pay_status != $new->pay_status) $result .= 'Статус оплаты => '. $new->pay_status."%0A";
//        if($last->doc_from != $new->doc_from) $result .= 'Период ЗД с => '. $new->doc_from."%0A";
//        if($last->doc_to != $new->doc_to) $result .= 'Период ЗД по => '. $new->doc_to."%0A";
        if($last->customer != $new->customer) $result .= 'Заказчик => '. $new->customer."%0A";

        /*'file' => 'Файл',
        'commentary' => 'Комментарии',*/
        return $result;
    }

    public function setChanging($old, $new, $status)
    {
        if($status == "Изменено"){
            $this->setToChangeTable('Техника',"",$new->technic_id->name,$status);
            $this->setToChangeTable('Название',"",$new->name,$status);
            $this->setToChangeTable('Объект',"",$new->objects_id->name,$status);
            $this->setToChangeTable('Статус',"",$new->status_id->name,$status);
            $this->setToChangeTable('Срок аренды с',"",$new->rental_period_begin,$status);
            $this->setToChangeTable('Срок аренды по',"",$new->rental_period_end,$status);
            $this->setToChangeTable('Количество смен',"",$new->shift,$status);
            $this->setToChangeTable('Количество часов',"",$new->hours,$status);
            $this->setToChangeTable('Исполнитель',"",$new->executor_id->name,$status);
            $this->setToChangeTable('Тип техники',"",$new->type_id->name,$status);
            $this->setToChangeTable('Подгруппа',"",$new->subgroup_id->name,$status);
            $this->setToChangeTable('Комментарий',"",$new->comment,$status);
            $this->setToChangeTable('Загрузка документа',"",$new->listfaie,$status);
            $this->setToChangeTable('Срок доставки с',"",$new->delivery_from,$status);
            $this->setToChangeTable('Срок доставки по',"",$new->delivery_to,$status);
            $this->setToChangeTable('Сумма аренды',"",$new->rent_sum,$status);
            $this->setToChangeTable('Сумма доставки',"",$new->delivery_sum,$status);
            $this->setToChangeTable('Номер договора',"",$new->contract_num,$status);
            $this->setToChangeTable('Количество',"",$new->count,$status);
            $this->setToChangeTable('Статус оплаты',"",$new->pay_status,$status);
//            $this->setToChangeTable('Период ЗД с ',"",$new->doc_from,$status);
//            $this->setToChangeTable('Период ЗД по ',"",$new->doc_to,$status);
            $this->setToChangeTable('Заказчик ',"",$new->customer,$status);

        }else{
            if($old->name != $new->name)
                $this->setToChangeTable('Название',$old->name,$new->name,$status);
            if($old->technic_id != $new->technic_id){
                $oldval = Technic::find()->where(['id' => $old->technic_id])->one();
                $newval = Technic::find()->where(['id' => $new->technic_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Техника',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Техника',$oldval->name,$newval->name,$status);
                }
            }
            if($old->objects_id != $new->objects_id){
                $oldval = Objects::find()->where(['id' => $old->objects_id])->one();
                $newval = Objects::find()->where(['id' => $new->objects_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Объект',' ',$newval->address,$status);
                }else{
                    $this->setToChangeTable('Объект',$oldval->name,$newval->address,$status);
                }
            }
            if($old->status_id != $new->status_id){
                $oldval = BidStatus::find()->where(['id' => $old->status_id])->one();
                $newval = BidStatus::find()->where(['id' => $new->status_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Статус',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Статус',$oldval->name,$newval->name,$status);
                }
            }
            if($old->rental_period_begin != $new->rental_period_begin)
                $this->setToChangeTable('Срок аренды с',$old->rental_period_begin,$new->rental_period_begin,$status);
            if($old->rental_period_end != $new->rental_period_end)
                $this->setToChangeTable('Срок аренды по',$old->rental_period_end,$new->rental_period_end,$status);
            if($old->shift != $new->shift)
                $this->setToChangeTable('Количество смен',$old->shift,$new->shift,$status);
            if($old->hours != $new->hours)
                $this->setToChangeTable('Количество часов',$old->hours,$new->hours,$status);
            if($old->executor_id != $new->executor_id){
                $oldval = Company::find()->where(['id' => $old->executor_id])->one();
                $newval = Company::find()->where(['id' => $new->executor_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Исполнитель',' ',$newval->name,$status);
                }else{
                    if($oldval && $newval){
                        $this->setToChangeTable('Исполнитель',$oldval->name,$newval->name,$status);
                    }
                }
            }
            if($old->executor_id != $new->executor_id){
                $oldval = Company::find()->where(['id' => $old->executor_id])->one();
                $newval = Company::find()->where(['id' => $new->executor_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Исполнитель',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Исполнитель',$oldval->name,$newval->name,$status);
                }
            }
            if($old->type_id != $new->type_id){
                $oldval = TechnicType::find()->where(['id' => $old->type_id])->one();
                $newval = TechnicType::find()->where(['id' => $new->type_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Тип техники',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Тип техники',$oldval->name,$newval->name,$status);
                }
            }
            if($old->subgroup_id != $new->subgroup_id){
                $oldval = TechnicTypeSubgroup::find()->where(['id' => $old->subgroup_id])->one();
                $newval = TechnicTypeSubgroup::find()->where(['id' => $new->subgroup_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Подгруппа',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Подгруппа',$oldval->name,$newval->name,$status);
                }
            }
            if($old->customer != $new->customer){
                $oldval = Company::find()->where(['id' => $old->customer])->one();
                $newval = Company::find()->where(['id' => $new->customer])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Заказчик',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Заказчик',$oldval->name,$newval->name,$status);
                }
            }
            if($old->delivery_from != $new->delivery_from)
                $this->setToChangeTable('Срок доставки с',$old->delivery_from,$new->delivery_from,$status);
            if($old->delivery_to != $new->delivery_to)
                $this->setToChangeTable('Срок доставки по',$old->delivery_to,$new->delivery_to,$status);
            if($old->rent_sum != $new->rent_sum)
                $this->setToChangeTable('Сумма аренды',$old->rent_sum,$new->rent_sum,$status);
            if($old->delivery_sum != $new->delivery_sum)
                $this->setToChangeTable('Сумма доставки',$old->delivery_sum,$new->delivery_sum,$status);
            if($old->contract_num != $new->contract_num)
                $this->setToChangeTable('Номер договора',$old->contract_num,$new->contract_num,$status);
            if($old->count != $new->count)
                $this->setToChangeTable('Количество',$old->count,$new->count,$status);
            if($old->pay_status != $new->pay_status)
                $this->setToChangeTable('Статус оплаты',$old->pay_status,$new->pay_status,$status);
//            if($old->doc_from != $new->doc_from)
//                $this->setToChangeTable('Период ЗД с',$old->doc_from,$new->doc_from,$status);
//            if($old->doc_to != $new->doc_to)
//                $this->setToChangeTable('Период ЗД по',$old->doc_to,$new->doc_to,$status);


            if($old->comment != $new->comment)
                $this->setToChangeTable('Комментарий',$old->comment,$new->comment,$status);
            if($old->listFile != $new->listFile) {
                $info = '';
                foreach ($new->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new BidHistory([
                    'user_id' => Yii::$app->user->identity->id,
                    'bid_id' => $this->id,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }

        }

    }
    public function setToChangeTable($field,$old_value,$new_value,$status)
    {
        $model = new BidHistory();
        $model->bid_id = $this->id;
        $model->created_at = date('Y-m-d H:i:s');
        $model->user_id = Yii::$app->user->identity->id;
        $model->history_comment = "Были изменения в '{$field}' с '{$old_value}' на '{$new_value}'";

        $model->save();
    }
}
