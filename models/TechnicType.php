<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "technic_type".
 *
 * @property int $id
 * @property string $name Вид Техники
 * @property array $subgroups подгруппы
 */
class TechnicType extends \yii\db\ActiveRecord
{
    public $subgroupsFld;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'technic_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['subgroupsFld',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Вид Техники',
            'subgroupsFld' => 'Подгруппа',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        $bankAll = TechnicTypeSubgroup::find()->where(['technic_type_id' => $this->id])->all();
        \Yii::warning($this->subgroupsFld, 'Subgroups');
        if($this->subgroupsFld){
            foreach ($bankAll as $sub) {
                $delete = true;
                foreach ($this->subgroupsFld as $subgroupFld)
                {
                    if($sub->id == $subgroupFld['id']){
                        $delete = false;
                    }
                }

                if($delete){
                    $sub->delete();
                }
            }
        } else {
            foreach ($bankAll as $sub){
                $sub->delete();
            }
        }

        if($this->subgroupsFld != null){
            foreach ($this->subgroupsFld as $item) {
                $bank = TechnicTypeSubgroup::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new TechnicTypeSubgroup([
                        'technic_type_id' => $this->id,
                        'name' => $item['name'],
                    ]))->save(false);
                } else {
                    $bank->name = $item['name'];
                    $bank->save(false);
                }
            }
        }
    }


    /**
     * @inheritdoc
     */


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubgroups()
    {
        return $this->hasOne(TechnicTypeSubgroup::className(), ['technic_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TechnicTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TechnicTypeQuery(get_called_class());
    }
}
