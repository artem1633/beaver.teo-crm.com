<?php

namespace app\models;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "characteristic".
 *
 * @property int $id
 * @property int $type_id Тип техники
 * @property int $subgroup_id Подгруппа
 * @property int $technic_id Техника
 * @property int $company_id Компания
 * @property string $file Загрузка фалйла
 * @property string $release_year Год выпуска
 * @property string $mileage Пробег
 * @property int $price_before Цена до 30 дней
 * @property int $price_after Цена свыше 30 дней
 * @property string $vin VIN номер
 * @property UploadedFile $upload
 * @property Company $company
 * @property TechnicTypeSubgroup $subgroup
 * @property Technic $technic
 * @property TechnicType $type
 */
class Characteristic extends \yii\db\ActiveRecord
{
    public $upload;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'characteristic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'subgroup_id', 'technic_id', 'company_id', 'price_before', 'price_after'], 'integer'],
            [['release_year','upload'], 'safe'],
            [['upload',],'file'],
            [['file', 'mileage', 'vin'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['subgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => TechnicTypeSubgroup::className(), 'targetAttribute' => ['subgroup_id' => 'id']],
            [['technic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Technic::className(), 'targetAttribute' => ['technic_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TechnicType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Тип техники',
            'subgroup_id' => 'Подгруппа',
            'technic_id' => 'Техника',
            'company_id' => 'Компания',
            'file' => 'Загрузка фалйла',
            'release_year' => 'Год выпуска',
            'mileage' => 'Пробег',
            'price_before' => 'Цена до 30 дней',
            'price_after' => 'Цена свыше 30 дней',
            'vin' => 'VIN номер',
            'upload' => 'Загрзка файла',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->upload != null){
            $fileName = Yii::$app->security->generateRandomString();
            if(is_dir('upload') == false){
                mkdir('upload');
            }
            $path = "uploads/{$fileName}.{$this->upload->extension}";
            $this->upload->saveAs($path);
            if($this->upload != null && file_exists($this->upload)){
                unlink($this->upload);
            }
            $this->file = $path;
        }

        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubgroup()
    {
        return $this->hasOne(TechnicTypeSubgroup::className(), ['id' => 'subgroup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnic()
    {
        return $this->hasOne(Technic::className(), ['id' => 'technic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TechnicType::className(), ['id' => 'type_id']);
    }

    /**
     * @inheritdoc
     * @return CharacteristicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CharacteristicQuery(get_called_class());
    }
}
