<?php

namespace app\models;

use app\base\CompanyActiveRecord;
//use components\CompanySettingInstance;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $email Email
 * @property string $last_name Фамилия
 * @property string $name Имя
 * @property string $patronymic Отчество
 * @property int $company_id Компания
 * @property string $phone Телефон
 * @property string $promo Промо-код
 * @property int $is_company_super_admin Является ли администратором компании
 * @property int $access Доступ (вкл/выкл)
 * @property int $role Роль
 * @property int $position_id Должность
 * @property string $password_hash Зашифрованный пароль
 * @property string $last_activity_datetime Дата и время последней активности
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $created_at
 * @property string $company_type

 * @property string $fio
 *
 * @property Ticket[] $tickets
 * @property Company $company
 * @property Position $position
 */
class User extends CompanyActiveRecord implements IdentityInterface, \rmrevin\yii\module\Comments\interfaces\CommentatorInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    public $password;
    public $company_type;
    private $oldPasswordHash;

    private $_companySetting;

    const ROLE_ADMIN = 0;
    const ROLE_MODERATOR = 1;
    const ROLE_USER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'last_name', 'patronymic', 'phone',  'position_id', 'company_id', 'email', 'is_deletable', 'password', 'password_hash'],
            self::SCENARIO_EDIT => ['name', 'last_name', 'patronymic', 'phone',  'position_id', 'company_id', 'email', 'is_deletable', 'password', 'password_hash'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'last_name'], 'required'],
            [['company_id', 'is_company_super_admin', 'access', 'role', 'position_id', 'is_deletable'], 'integer'],
            [['last_activity_datetime', 'created_at'], 'safe'],
			[['promo'], 'string', 'max' => 255],
            [['email', 'last_name', 'name', 'patronymic', 'phone', 'password_hash', 'avatar'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['position_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->email}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }
    }

    public function sendEmailMessage($subject, $htmlBody)
    {
        try{
            Yii::$app->mailer->compose()
                ->setFrom('beaver@shop-crm.ru')
                ->setTo($this->email)
                ->setSubject($subject)
                ->setHtmlBody($htmlBody)
                ->send();
        } catch(\Exception $e){
            Yii::warning($e);
        }
    }

    public function getRealAvatarPath()
    {
        return $this->avatar != null ? $this->avatar : 'img/nouser.png';
    }

    public function getCommentatorAvatar()
    {
        return '';
    }

    public function getCommentatorName()
    {
        return $this->name;
    }

    public function getCommentatorUrl()
    {
        return ['/profile', 'id' => $this->id];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }

    /**
     * @return string
     */
    public function getFio()
    {
        return "{$this->last_name} {$this->name} {$this->patronymic}";
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }


            if($this->avatar) {

            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'last_name' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'company_id' => 'Компания',
            'phone' => 'Телефон',
			'promo' => 'Промо-код',
            'is_company_super_admin' => 'Является ли администратором компании',
            'access' => 'Доступ (вкл/выкл)',
            'role' => 'Роль',
            'position_id' => 'Должность',
            'password' => 'Пароль',
            'password_hash' => 'Зашифрованный пароль',
            'last_activity_datetime' => 'Дата и время последней активности',
            'is_deletable' => 'Можно удалить или нельзя',
            'created_at' => 'Дата и время создания',
            'avatar' => 'Аватар',
            'company_type' => 'Тип компании'
        ];
    }


    /**
     * Суперадмин или нет
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->company_id == 1 && $this->is_company_super_admin == 1;
    }

    /**
     * Админ компании или нет
     * @return bool
     */
    public function isCompanyAdmin()
    {
        return $this->is_company_super_admin == 1;
    }

//    /**
//     * @return CompanySetting
//     */
//    public function getCompanySetting()
//    {
//        $setting = CompanySetting::find()->where(['company_id' => $this->company_id])->one();
//
//        if($setting == null){
//            $setting = new CompanySetting([
//                'company_id' => $this->company_id
//            ]);
//            $setting->save();
//        }
//
//        return $setting;
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    public function getCompanyName()
    {
        $company = $this->hasOne(Company::className(), ['id' => 'company_id']);
        return $company->name;
    }
    public function getName()
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public static function find()
    {
        return new UserQuery(get_called_class());
    }

}
