<?php

namespace app\models;

use app\components\MyUploadedFile;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scan".
 *
 * @property int $id
 * @property string $name название
 * @property string $loaded_at дата и время загрузки
 * @property string $link ссылка на файл
 * @property int $author_id кто загрузил
 * @property int $bid_id id заявки
 *
 * @property User $author
 * @property Company $company
 */
class Scan extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $listFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'loaded_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'author_id',
                'value' => Yii::$app->user->id
            ],
//            [
//                'class' => BlameableBehavior::class,
//                'updatedByAttribute' => null,
//                'createdByAttribute' => 'company_id',
//                'value' => Yii::$app->user->identity->company_id
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loaded_at'], 'safe'],
            [['author_id', 'bid_id'], 'integer'],
            [['name', 'link'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'название',
            'loaded_at' => 'дата и время загрузки',
            'link' => 'ссылка на файл',
            'author_id' => 'кто загрузил',
            'bid_id' => 'id заявки',
            'company_id' => 'Компания',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->listFile != null){
            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file){
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'bid_id' => $this->bid_id,
                    'author_id' => Yii::$app->user->getId(),
                ]);
                $scan->save(false);
            }
        }
    }
    public function beforeDelete()
    {
        if($this->bid_id != null){
            (new BidHistory([
                'user_id' => Yii::$app->user->identity->id,
                'bid_id' => $this->bid_id,
                'history_comment' => 'Был удален документ: '.$this->name,
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }
        return parent::beforeDelete();
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @inheritdoc
     * @return SQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SQuery(get_called_class());
    }
}
