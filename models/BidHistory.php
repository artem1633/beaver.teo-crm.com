<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bid_history".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $bid_id Id Заявки
 * @property string $history_comment Содержание действия
 * @property string $created_at
 *
 * @property Bid $bid
 * @property User $user
 */
class BidHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'bid_id'], 'integer'],
            [['history_comment'], 'string'],
            [['created_at'], 'safe'],
            [['bid_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bid::className(), 'targetAttribute' => ['bid_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'bid_id' => 'Id Заявки',
            'history_comment' => 'Содержание действия',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBid()
    {
        return $this->hasOne(Bid::className(), ['id' => 'bid_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return BidHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BidHistoryQuery(get_called_class());
    }
}
