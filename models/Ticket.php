<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $subject Заголовок
 * @property string $description Описание
 * @property string $status Статус
 * @property int $is_read Прочитано
 * @property string $last_message_datetime Дата и время последнего сообщения
 * @property string $created_at Дата и время создания
 * @property string $closed_datetime Дата и время закрытия
 *
 * @property User $user
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_WORK = 1;
    const STATUS_DONE = 2;
    const STATUS_REJECTED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_read'], 'integer'],
            [['description'], 'string'],
            [['last_message_datetime', 'created_at', 'closed_datetime'], 'safe'],
            [['subject', 'status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'subject' => 'Заголовок',
            'description' => 'Описание проблемы',
            'status' => 'Статус',
            'is_read' => 'Прочитано',
            'last_message_datetime' => 'Дата и время последнего сообщения',
            'created_at' => 'Дата и время создания',
            'closed_datetime' => 'Дата и время закрытия',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $this->user_id = Yii::$app->user->identity->id;
        }

        if($this->isNewRecord){
            $this->created_at = date("Y/m/d H:i:s");
            $users = User::find()->where(['=', 'company_id', 1])->all();
            foreach ($users as $user){
                $subject =  "Добавлен новый тикет";
                $htmlBody = "<p>Заголовок «{$this->subject}» </p><p>Описание проблемы:</p><p>{$this->description}</p>> ";
                $user->sendEmailMessage($subject,$htmlBody);
            }
        }

        return parent::beforeSave($insert);
    }


  
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнено',
            self::STATUS_REJECTED => 'Отклонен',
        ];
    }
}
