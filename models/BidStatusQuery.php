<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[BidStatus]].
 *
 * @see BidStatus
 */
class BidStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return BidStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BidStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
