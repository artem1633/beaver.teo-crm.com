<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bid;

/**
 * BidSearch represents the model behind the search form about `app\models\Bid`.
 */
class BidSearch extends Bid
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'objects_id', 'status_id', 'shift', 'hours', 'created_by', 'executor_id','type_id','technic_id','subgroup_id','customer', 'created_by_user_id'], 'integer'],
            [['name', 'rental_period_begin', 'rental_period_end', 'created_at', 'delivery_from', 'delivery_to','contract_num'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

//        if(Yii::$app->session->has('bid-search-session')){
//            $this->attributes = Yii::$app->session->get('bid-search-session');
//        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $session = false)
    {
        $query = Bid::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if($session){
            Yii::$app->session->set('bid-search-session', $this->attributes);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'technic_id' => $this->technic_id,
            'objects_id' => $this->objects_id,
            'status_id' => $this->status_id,
            'customer' => $this->customer,
            'sn_num' => $this->sn_num,
//            'rental_period_begin' => $this->rental_period_begin,
//            'rental_period_end' => $this->rental_period_end,
            'delivery_from' => $this->delivery_from,
            'delivery_to' => $this->delivery_to,
            'shift' => $this->shift,
            'hours' => $this->hours,
            'executor_id' => $this->executor_id,
            'type_id' => $this->type_id,
            'subgroup_id' => $this->subgroup_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_user_id' => $this->created_by_user_id,
            'contract_num' => $this->doc_num,
            'count' => $this->count,
            'comment' => $this->comment,
            'pay_status' => $this->pay_status,
//            'doc_from' => $this->doc_from,
//            'doc_to' => $this->doc_to
        ]);

//        if($this->rental_period_begin){
//            $rentalPeriodBegin = implode('-', array_reverse(explode('.', $this->rental_period_begin)));
        $query->andFilterWhere(['>=', 'rental_period_begin', $this->rental_period_begin]);
//        }

//        if($this->rental_period_end){
//            $rentalPeriodEnd = implode('-', array_reverse(explode('.', $this->rental_period_end)));
        // $query->andFilterWhere(['<=', 'rental_period_end', $this->rental_period_end]);
//        }

        // $query->andFilterWhere(
        //     [
        //         'and',
        //         ['>=', 'rental_period_end', $this->rental_period_begin],
        //         ['<=', 'rental_period_end', $this->rental_period_end],
        //     ]
        // );

        $query->andWhere(['!=', 'status_id', 4]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
