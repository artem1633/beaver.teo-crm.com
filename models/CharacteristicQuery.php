<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Characteristic]].
 *
 * @see Characteristic
 */
class CharacteristicQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Characteristic[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Characteristic|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
