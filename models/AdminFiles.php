<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_files".
 *
 * @property int $id
 * @property string $contract_creator Договор создателя
 * @property string $contract_winner Договор победителя
 */
class AdminFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contract_creator', 'contract_winner'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_creator' => 'Договор создателя',
            'contract_winner' => 'Договор победителя',
        ];
    }
}
