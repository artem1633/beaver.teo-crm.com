<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bid_status".
 *
 * @property int $id
 * @property string $name Название
 * @property string $color Цвет
 * @property int $style Стиль
 *
 * @property Bid[] $bs
 */
class BidStatus extends \yii\db\ActiveRecord
{
    const STYLE_DEFAULT = 0;
    const STYLE_DOTTED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
            [['style'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'color' => 'Цвет',
            'style' => 'Стиль',
        ];
    }

    /**
     * @return array
     */
    public static function getStyleLabels()
    {
        return [
            self::STYLE_DEFAULT => 'Обычный',
            self::STYLE_DOTTED => 'Пунктир',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBs()
    {
        return $this->hasMany(Bid::className(), ['status_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BidStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BidStatusQuery(get_called_class());
    }
}
