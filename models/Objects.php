<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property string $address Адрес объекта
 * @property string $name Название объекта
 * @property string $contact_name Контактное лицо
 * @property integer $company_id Компания
 * @property string $contact_phone Тел. контактного лицв
 * @property string $access_time Время работы/доступа
 *
 */
class Objects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'objects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'],'integer'],
            [['address', 'contact_name', 'contact_phone', 'access_time', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес объекта',
            'contact_name' => 'Контактное лицо',
            'contact_phone' => 'Тел. контактного лицв',
            'access_time' => 'Время работы/доступа',
            'company_id' => 'Компания',
            'name' => 'Название объекта'
        ];
    }

    public function beforeSave($insert)
    {

        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }



    /**
     * @inheritdoc
     * @return ObjectsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ObjectsQuery(get_called_class());
    }
}
