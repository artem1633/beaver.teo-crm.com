<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property string $num Номер счета
 * @property string $invoice_date Дата счета
 * @property string $status Статус оплаты
 * @property string $pay_date Дата оплаты
 * @property int $bid_id id заявки
 * @property int $date_from период с
 * @property int $date_to период по
 * @property int $upd_date дата упд
 * @property int $upd_num номер упд
 * @property int $sum сумма счета
 * @property int $company_id Компания
 * @property double $sum_not_payed Не оплачено
 * @property int $delivery Доставка
 *
 * @property Bid $bid
 * @property Company $company
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            [
//                'class' => BlameableBehavior::class,
//                'updatedByAttribute' => null,
//                'createdByAttribute' => 'company_id',
//                'value' => Yii::$app->user->identity->company_id
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_date', 'pay_date' ,'date_from' , 'date_to', 'upd_date', 'upd_num','sum'], 'safe'],
            [['bid_id', 'delivery'], 'integer'],
            [['num', 'status'], 'string', 'max' => 255],
            [['sum_not_payed'], 'number'],
            [['bid_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bid::className(), 'targetAttribute' => ['bid_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'num' => 'Номер счета',
            'invoice_date' => 'Дата счета',
            'status' => 'Статус оплаты',
            'pay_date' => 'Дата оплаты',
            'bid_id' => 'id заявки',
            'date_from' => 'Период от',
            'date_to' => 'Период по',
            'upd_date' => 'Дата УПД',
            'upd_num' => 'Номер УПД',
            'sum' => 'Сумма счета',
            'company_id' => 'Компания',
            'sum_not_payed' => 'Не оплачено',
            'delivery' => 'Доставка',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if($this->bid_id != null){
            (new BidHistory([
                'user_id' => Yii::$app->user->identity->id,
                'bid_id' => $this->bid_id,
                'history_comment' => 'Был удален счет: '.$this->num,
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->bid_id != null){
            (new BidHistory([
                'user_id' => Yii::$app->user->identity->id,
                'bid_id' => $this->bid_id,
                'history_comment' => 'Был добавлен счет: '.$this->num,
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
//            $this->status = 'не оплачено';
        }

        if ($this->pay_date != null){
            $this->status = 'оплачено';
        }
        if ($this->upd_date != null){
            $this->status = 'закрыт';
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            $bid = $this->bid;
            $customer = Company::findOne($bid->customer);
            if($customer){
                $customerUser = User::find()->where(['company_id' => $customer->id, 'is_company_super_admin' => true])->one();
                if($customerUser){
                    $customerUser->sendEmailMessage(
                        'Новый счет',
                        'Вам выставлен новый счет №'.$this->id
                    );
                }
            }
        }


        $this->bid->status_id = 2;
        $this->bid->save(false, null, true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBid()
    {
        return $this->hasOne(Bid::className(), ['id' => 'bid_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @inheritdoc
     * @return InvoiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InvoiceQuery(get_called_class());
    }
}
