<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Characteristic;

/**
 * CharacteristicSearch represents the model behind the search form about `app\models\Characteristic`.
 */
class CharacteristicSearch extends Characteristic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'subgroup_id', 'technic_id', 'company_id', 'price_before', 'price_after'], 'integer'],
            [['file', 'release_year', 'mileage', 'vin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Characteristic::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->type_id,
            'subgroup_id' => $this->subgroup_id,
            'technic_id' => $this->technic_id,
            'company_id' => $this->company_id,
            'release_year' => $this->release_year,
            'price_before' => $this->price_before,
            'price_after' => $this->price_after,
        ]);

        $query->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'mileage', $this->mileage])
            ->andFilterWhere(['like', 'vin', $this->vin]);

        return $dataProvider;
    }
}
