<?php

namespace app\models\forms;

use app\models\Bid;
use app\models\BidHistory;
use Yii;
use yii\base\Model;
use app\components\MyUploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Scan;

/**
 * Class ScanForm
 * @package app\models\forms
 */
class ScanForm extends Model
{
    /**
     * @var array
     */
    public $listFile;

    /**
     * @var int
     */
    public $bidId;

    /**
     * @var int
     */
    public $companyId;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['listFile'], 'safe'],
            [['bidId', 'companyId'], 'integer'],
        ];
    }

    /**
     * @return boolean
     */
    public function loadFile()
    {
        if($this->listFile != null){

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file){
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'bid_id' => $this->bidId,
                    'author_id' => Yii::$app->user->getId(),
                    'company_id' => $this->companyId,
                ]);

                $scan->save(false);
            }

            if($this->bidId != null){
                $info = '';
                foreach ($this->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new BidHistory([
                    'user_id' => Yii::$app->user->identity->id,
                    'bid_id' => $this->bidId,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }

        }

        return true;
    }
}