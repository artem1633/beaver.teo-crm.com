<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "technic".
 *
 * @property int $id
 * @property string $name Название Техники
 * @property string $model Модель 
 * @property string $characteristics Техническе характеристики
 * @property string $equipment Останстка
 * @property integer $type_id Тип
 * @property integer $subgroup_id подгруппа
 *

 */
class Technic extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'technic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id','subgroup_id'], 'integer'],
            [['name', 'model', 'characteristics', 'equipment'], 'string', 'max' => 255],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название, модель',
            'model' => 'Сдается с оператором',
            'characteristics' => 'Pdf',
            'file' => 'Pdf',
            'equipment' => 'Комплектация, доп. оборудование',
            'type_id' => 'Тип техники',
            'subgroup_id' => 'Подгруппа типа'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->file){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();

            $path = "uploads/{$name}.{$this->file->extension}";

            $this->file->saveAs($path);

            $this->characteristics = $path;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubgroup()
    {
        return $this->hasOne(TechnicTypeSubgroup::className(), ['id' => 'subgroup_id']);
    }
    public function getType()
    {
        return $this->hasOne(TechnicType::className(), ['id' => 'type_id']);
    }
    public function getSubs()
    {
        $list = [];
        $type = TechnicType::find()->all();
        foreach ($type as $item) {
            $subs = TechnicTypeSubgroup::find()->where(['technic_type_id' => $item->id])->all();
            foreach ($subs as $sub){
                $list[$sub->name] = ArrayHelper::map(TechnicTypeSubgroup::find()->all(), 'id', 'name');
            }
        }

        return $list;
    }

    /**
     * @inheritdoc
     * @return TechnicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TechnicQuery(get_called_class());
    }
}
