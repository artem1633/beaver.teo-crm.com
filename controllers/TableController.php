<?php


namespace app\controllers;



use app\models\BidSearch;
use app\models\Company;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

class TableController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex(){
        $searchModel = new BidSearch();

        if(Yii::$app->session->has('bid-search-session')){
            $searchModel->attributes = Yii::$app->session->get('bid-search-session');
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
//        $dataProvider->query->andWhere(['status_id' => 1]);

        if(Yii::$app->user->identity->isSuperAdmin()){
//            $dataProvider->query->andWhere(['']);
        } else {
            if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
                $dataProvider->query->andWhere(['created_by' => Yii::$app->user->identity->id]);
            } else if(Yii::$app->user->identity->company->type == Company::TYPE_PROVIDER){
                $dataProvider->query->andWhere(['executor_id' => Yii::$app->user->identity->id]);
            }
        }



//        echo $dataProvider->query->createCommand()->getRawSql();
//        exit;

        if($searchModel->rental_period_begin == null && $searchModel->rental_period_end == null){
//            echo '1';
//            exit;

            // $dataProvider->query->andFilterWhere(['>=', 'rental_period_begin', date('Y-m-01')]);
            // $dataProvider->query->andFilterWhere(['<=', 'rental_period_end', date('Y-m-t')]);

            // $dataProvider->query->andFilterWhere(['or',
            //     [
            //         'and',
            //         ['>=', 'rental_period_begin', date('Y-m-01')],
            //         ['<=', 'rental_period_end', date('Y-m-t')],
            //     ],
            //     [
            //         'and',
            //         ['>=', 'rental_period_end', date('Y-m-01')],
            //         ['<=', 'rental_period_end', date('Y-m-t')],
            //     ]
            // ]);

            $dataProvider->query->andFilterWhere(
                [
                    'and',
                    ['>=', 'rental_period_end', date('Y-m-01')],
                    ['<=', 'rental_period_end', date('Y-m-t')],
                ]
            );
        }

        $dataProvider->pagination = false;



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClearSession()
    {
        Yii::$app->session->remove('bid-search-session');
    }
}