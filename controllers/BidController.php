<?php

namespace app\controllers;

use app\models\BidHistorySearch;
use app\models\BidStatus;
use app\models\Characteristic;
use app\models\Company;
use app\models\Invoice;
use app\models\InvoiceSearch;
use app\models\Objects;
use app\models\ObjectsSearch;
use app\models\ScanSearch;
use app\models\Technic;
use app\models\TechnicTypeSubgroup;
use app\models\Timesheet;
use app\models\TimesheetSearch;
use app\models\User;
use app\models\UserSearch;
use DateTime;
use MongoDB\BSON\Timestamp;
use Yii;
use app\models\Bid;
use app\models\BidSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * BidController implements the CRUD actions for Bid model.
 */
class BidController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Bid models.
     * @return mixed
     */
    public function actionIndex()
    {

        if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 0){
            $searchModel = new BidSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['created_by' => Yii::$app->user->identity->company_id]);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        if(Yii::$app->user->identity->isSuperAdmin() == false and Yii::$app->user->identity->company->type == 1){
            $technicLike = ArrayHelper::getColumn(Characteristic::find()
                ->where(['company_id' => Yii::$app->user->identity->company_id])->all(),'technic_id');

            $searchModel = new BidSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//            $dataProvider->query->orFilterWhere(['and',['in','technic_id',$technicLike],['is','executor_id',null]]);
//            $dataProvider->query->orFilterWhere(['executor_id' => Yii::$app->user->identity->company->id]);
            $dataProvider->query->andFilterWhere(['or', ['and',['in','technic_id',$technicLike],['is','executor_id',null]], ['executor_id' => Yii::$app->user->identity->company->id]]);

            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];


            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        $searchModel = new BidSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetExecutorsByTechnic($id)
    {
        $companiesPks = ArrayHelper::getColumn(\app\models\Characteristic::find()->where(['technic_id' => $id])->all(), 'company_id');
        $executors = Company::find()->where(['id' => $companiesPks, 'type' => 1])->all();

        $output = '';
        foreach ($executors as $executor)
        {
            $output .= "<option value='{$executor->id}'>{$executor->name}</option>";
        }

        return $output;
    }


    /**
     * Displays a single Bid model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);


        $timesheet_model = Timesheet::find()->forBid($model->id)->all();

//        $author_model = $author_model = User::find()->where(['id' => $model->created_by])->one();
        $executorModel = Company::find()->where(['id' => $model->executor_id])->one();
        $customerModel = Company::find()->where(['id' => $model->customer])->one();


// //        VarDumper::dump($timesheet_model, 10, true);
//        VarDumper::dump(count($timesheet_model), 10, true);
//        VarDumper::dump(ArrayHelper::getColumn($timesheet_model, 'date'), 10, true);
//        exit;

        $clientUser = User::findOne($model->customer);

        $invoiceSearchModel = new InvoiceSearch();
        $invoiceDataProvider = $invoiceSearchModel->search([]);
        $invoiceDataProvider->query->andWhere(['bid_id' => $id, 'company_id' =>  $model->customer]);

        $executorUser = User::findOne($model->executor_id);

        $invoiceProviderSearchModel = new InvoiceSearch();
        $invoiceProviderDataProvider = $invoiceProviderSearchModel->search([]);
        $invoiceProviderDataProvider->query->andWhere(['bid_id' => $id, 'company_id' => $model->executor_id]);

        if(Yii::$app->user->identity->role != User::ROLE_ADMIN){
            $invoiceDataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        $bidHistorySearchModel = new BidHistorySearch();
        $bidHistoryDataProvider = $bidHistorySearchModel->search([]);
        $bidHistoryDataProvider->query->andWhere(['bid_id' => $id]);
        $bidHistoryDataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $scanSearchModel = new ScanSearch();
        $scanDataProvider = $scanSearchModel->search([]);
        $scanDataProvider->query->andWhere(['bid_id' => $id, 'company_id' =>  $model->customer]);

        $scanProviderSearchModel = new ScanSearch();
        $scanProviderDataProvider = $scanProviderSearchModel->search([]);
        $scanProviderDataProvider->query->andWhere(['bid_id' => $id, 'company_id' => $model->executor_id]);


//        var_dump($scanDataProvider->models);
//        exit;




        if(Yii::$app->user->identity->role != User::ROLE_ADMIN){
            $scanDataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'invoiceSearchModel' => $invoiceSearchModel,
            'invoiceDataProvider' => $invoiceDataProvider,
            'invoiceProviderSearchModel' => $invoiceProviderSearchModel,
            'invoiceProviderDataProvider' => $invoiceProviderDataProvider,
            'scanProviderSearchModel' => $scanProviderSearchModel,
            'scanProviderDataProvider' => $scanProviderDataProvider,
            'scanSearchModel' => $scanSearchModel,
            'scanDataProvider' => $scanDataProvider,
            'bidHistorySearchModel' => $bidHistorySearchModel,
            'bidHistoryDataProvider' => $bidHistoryDataProvider,
            'timesheet_model' => $timesheet_model,
            'executor_model' => $executorModel,
            'customer_model' => $customerModel
        ]);

    }

    public function actionGetFormData($startDate, $endDate, $provider_rent_sum)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $datetime1 = new Datetime("$startDate");
        $datetime2 = new Datetime("$endDate");
        $datetime2->modify('+1 day');
        $shift = $datetime1->diff($datetime2)->days;

        $payed = 0;

        $payedDays = 0;

        if($provider_rent_sum != null){
            $totalDays = $shift;
            $payedDays = round($payed / $provider_rent_sum);
        } else {
            $payedDays = 0;
        }

        return ['payed' => $payedDays, 'not_payed' => ($shift - $payedDays)];
    }


    /**
     * Creates a new Bid model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Bid();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить заявку",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'action' => 'create',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавление заявки",
                    'content'=>'<span class="text-success">Добавлено успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Добавить еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить заявку",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'action' => 'create',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'action' => 'create',
                ]);
            }
        }

    }

    /**
     * Updates an existing Bid model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменть заявку # ".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'action' => 'update',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){

//                var_dump($model);
//                exit;

                return [
                    'forceReload'=> $containerPjaxReload,
                    'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Измененить заявку # ".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'action' => 'update',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'action' => 'update',
                ]);
            }
        }
    }

//    public function actionClone($id,$containerPjaxReload = '#pjax-container-info-container'){
//        $oldmodel=$this->findModel($id);
//        $model =  new Bid;
//        $model->attributes = $oldmodel->attributes;
//        $model->id = null;
//        $model->save();
//        $findId = Bid::find()->orderBy(['id' => SORT_DESC])->one();
//        return $this->redirect(['index']);
//    }

    public function actionClone($id,$containerPjaxReload = '#pjax-container-info-container'){
        $request = Yii::$app->request;
        $model = new Bid();
        $copyModel = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){

                $model->attributes = $copyModel->attributes;

                return [
                    'title'=> "Добавить заявку",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'action' => 'create',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$containerPjaxReload,
                    'title'=> "Добавление заявки",
                    'content'=>'<span class="text-success">Добавлено успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Добавить еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить заявку",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'action' => 'create',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'action' => 'create',
                ]);
            }
        }
    }

    /**
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionUpdateHours()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['tsId']) == false || isset($_POST['hours']) == false){
            throw new BadRequestHttpException();
        }

        $tsId = $_POST['tsId'];
        $hours = $_POST['hours'];

        $model = Timesheet::findOne($tsId);
        if(Yii::$app->user->identity->company->type == Company::TYPE_BUYER){
            $model->hours = $hours;
        } else {
            $model->provider_hours = $hours;
        }
        $model->save(false);

        return ['success' => true];
    }

    /**
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionUpdateHoursAdmin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['tsId']) == false || isset($_POST['hours']) == false || isset($_POST['type']) == false){
            throw new BadRequestHttpException();
        }

        $tsId = $_POST['tsId'];
        $hours = $_POST['hours'];
        $type = $_POST['type'];


        $model = Timesheet::findOne($tsId);
        if($type == 1){
            $model->hours = $hours;
        } else if($type == 2) {
            $model->provider_hours = $hours;
        }
        $model->save(false);

        return ['success' => true];
    }


    /**
     * Delete an existing Bid model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Bid model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys

        $status = BidStatus::find()->where(['name' => 'Удалена (Архив)'])->one();

        if($status == null){
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }

        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->status_id = $status->id;
            $model->save(false);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }


    public function actionSessionForm()
    {
        Yii::$app->session->set('bid-form-session', ArrayHelper::getValue($_POST, 'Bid'));
    }

    public function actionTest()
    {
        VarDumper::dump(Yii::$app->session->get('bid-form-session'), 10, true);
    }

    /**
     * Creates a new Objects model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateObject($id = null)
    {
        $request = Yii::$app->request;
        $model = new Objects();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить объект",
                    'content'=>$this->renderAjax('@app/views/objects/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                if($id){
                    $bid = $this->findModel($id);
                } else {
                    $bid = (new Bid());
                }

                if(Yii::$app->session->has('bid-form-session')){
                    \Yii::warning(Yii::$app->session->get('bid-form-session'));
                    $bid->attributes = Yii::$app->session->get('bid-form-session');
                    $bid->objects_id = $model->id;
                    Yii::$app->session->set('bid-form-session', null);
                }

                return [
                    'title'=> "Добавить заявку",
                    'content'=>$this->renderAjax($id ? 'update' : 'create', [
                        'model' => $bid,
                        'action' => $id ? 'update' : 'create',
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else{

                return [
                    'title'=> "Добавить объект",
                    'content'=>$this->renderAjax('@app/views/objects/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('@app/views/objects/create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Delete multiple existing Bid model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkClose()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys

        $status = BidStatus::find()->where(['name' => 'Закрыта'])->one();

        if($status == null){
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }

        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->status_id = $status->id;
            $model->save(false);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionSearchKind($q){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = ArrayHelper::map(TechnicTypeSubgroup::find()->where(['technic_type_id' => $q])->all(), 'id', 'name');
        return $list;
    }
    /**
     * @param string $q
     * @return array
     */
    public function actionSearchTech($q){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = ArrayHelper::map(Technic::find()->where(['subgroup_id' => $q])->all(), 'id', 'name');
        return $list;
    }

    public function actionSaveStructure()
    {
        $request = Yii::$app->request;

        $attribute_name = $request->post('name');
        $value = $request->post('value');
        $id = $request->post('id');

        if ($id && $attribute_name && $value){
            $model = Timesheet::findOne($id);
            $model->$attribute_name = $value;
            $model->save();
            if (!$model->save()){
                Yii::error($model->errors, '_error');
                return 'Ошибка сохранения';
            }
        }

        return 'Сохранено';
    }
    /**
     * Finds the Bid model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bid the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bid::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
