$('#btn-dropdown_header').click(function(){
    if($(".dropdown-menu").is(':visible')){
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});

var menuOpened = false;

$('#btn-user-dropdown').click(function(){
    $('#dropdown-user-menu').toggle();
});


$("[data-click='top-menu-toggled']").click(function(){
    if(menuOpened == false){
        $('#sidebar').css('left', 'unset');
        menuOpened = true;
    } else {
        $('#sidebar').css('left', '-220px');
        menuOpened = false;
    }
});

var socket = io('http://debtprice.teo-crm.com:3002?userId='+$('meta[name="user_id"]').attr('content'));

socket.on('update-rates', function(data){
    console.log('123');
    $.pjax.reload('#rate-io-pjax');
});

$('#generate-key').click(function(){
    $.get(
        '/company/change-api-key',
        function(response){
            $('#api-key').html(response);
        }
    )
});


initTabletPicker();

$('.multiple-input').on('afterAddRow', function(){
    initTabletPicker();
});

$('#ajaxCrudModal').on('shown.bs.modal', function() {
    setTimeout(function(){
        initTabletPicker();

        $('.multiple-input').on('afterAddRow', function(){
            initTabletPicker();
        });
    }, 500);
});


function initTabletPicker()
{
    // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $('[type="date"]').each(function(){
        $(this).data('value', $(this).val());
        $(this).attr('placeholder', 'Выберите дату');
    });

    $('[type="date"]').pickadate({
        format: 'dd.mm.yyyy',
        formatSubmit: 'yyyy-mm-dd'
    });
    // }
}